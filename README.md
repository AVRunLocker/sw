# AVR unLocker
 * It is IMPORTAT to DISABLE JTAG DEBUG on AVR! Please set fuses:

```
High fuse: 0xD9
Low fuse: 0xE1
```

 * This just disable JTAG and set internal oscillator to 1MHz)
 * Makefile is prepared for AVR dragon, however if You want just compile source
   code this makefile should work. Just type "make".
 * If You use USBasp programmer and Linux with avrdude, You can use prepared
   shell scripts. To load program just type:

```bash
$ ./compile_and_run_C.sh
```

 * or

```bash
$ make program
```

 * To set correctly fuses type (If You use USBasp):

```bash
$ ./load_fuses.sh
```

 * Anyway in makefile and scripts there is variable (programmer) which define 
   programmer name. So if you use USBasp just change this varable. You can't
   miss it.
 * Enjoy unLocked chips ;-)

  `Martin Stejskal`

###### martin.stej@gmail.com
