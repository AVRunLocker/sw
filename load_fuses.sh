#!/bin/bash
# Avrdude programmer name. Eq: usbasp, dragon_isp, jtag1 and so on
programmer=usbasp

echo "In default set CPU frequency to 1 MHz. If any argument is given then"
echo "frequency is set to 2 MHz"
echo "--------------------------------------------------------------------"

if $1;
then
  # For clock 1MHz - default in release version
  echo "Setting CPU frequency to 1 MHz"
  avrdude -c $programmer -p m32 -U lfuse:w:0xe1:m -U hfuse:w:0xd9:m
else
  # For clock 2MHz
  echo "Setting CPU frequency to 2 MHz"
  avrdude -c $programmer -p m32 -U lfuse:w:0xe2:m -U hfuse:w:0xd9:m
fi


