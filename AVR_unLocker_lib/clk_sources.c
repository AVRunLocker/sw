/**
 * \file
 *
 * \brief Allow connect selected clock source to target chip
 *
 * Created  21.06.2014\n
 * Modified 21.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#include "clk_sources.h"

//================================| Functions |================================
/* In dependency of used oscillator generate software CLK pulses to the target
 * chip if needed
 */
void give_CLK_pulse( uint8_t osc_type )
{
  // Test for internal oscillator
  if ( osc_type == INT_OSC )
    {
      _delay_ms(1);     // Wait for case, that target has slow oscillator
      return;           // Nothing else to do
    }

  // Test for external clock
  if ( osc_type == EXT_CLK )
    {
      // Need to generate some pulses, so for is used
      for ( uint8_t pulse_counter = num_ext_CLK_pulses;
            pulse_counter>0 ; pulse_counter--)
        {
          EXT_CLK_1;            // Set output to 1
          _delay_us( 10 );
          EXT_CLK_0;            // Set output to 0
          _delay_us( 10 );
        }
      return;   // Nothing else to do
    }

  // Test for external clock, but give just 1 pulse
  if ( osc_type == EXT_CLK_1_pulse )
  {
    EXT_CLK_1;
    _delay_us( 100 );
    EXT_CLK_0;
    _delay_us( 100 );
    return;
  }

  // Test for low frequency oscillator
  if ( osc_type == EXT_LF_OSC )
    {
      // Again is needed to generate some pulses. Analogy with EXT_CLK
      for ( uint8_t pulse_counter = num_ext_LF_OSC_pulses;
            pulse_counter>0 ; pulse_counter--)
        {
           EXT_LF_RES_1;        // Set output to 1
           _delay_us( 500 );
           EXT_LF_RES_0;        // Set output to 0
           _delay_us( 500 );
        }
    }

  // Else there is not needed to do anything

}

/*-----------------------------------------*/

/* Disconnect any oscillators connected to the target chip (not needed when
 * internal oscillator running)
 */
uint8_t Try_internal_oscillator(void)
{
  // Set CLK pin as input
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_in  );

  EXT_XTAL_0;   // Disconnect external XTAL
  EXT_RC_0;     // Disconnect external RC oscillator
  EXT_LF_RES_0; // Disconnect external Low Frequency oscillator

  return INT_OSC;
}

/*-----------------------------------------*/

/* Connect target chip to the external XTAL (4 MHz). Return oscillator type
 */
uint8_t Try_external_XTAL(void)
{
  // Set CLK pin as input
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_in  );

  EXT_XTAL_1;   // Connect external XTAL
  EXT_RC_0;     // Disconnect external RC oscillator
  EXT_LF_RES_0; // Disconnect external Low Frequency oscillator

  return EXT_XTAL_OSC;
}

/*-----------------------------------------*/

/* Connect target chip to the RC oscillator. Return oscillator type
 */
uint8_t Try_external_RC(void)
{
  // Set CLK pin as input
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_in  );

  EXT_XTAL_0;   // Disconnect external XTAL
  EXT_RC_1;     // Connect external RC oscillator
  EXT_LF_RES_0; // Disconnect external Low Frequency oscillator

  return EXT_RC_OSC;
}

/*-----------------------------------------*/

/* Set CLK pin as output and disable others oscillators. Pulses must be done by
 * software. Return oscillator type
 */
uint8_t Try_external_CLK(void)
{
  // Set CLK pin as output
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_out );

  EXT_XTAL_0;   // Disconnect external XTAL
  EXT_RC_0;     // Disconnect external RC oscillator
  EXT_LF_RES_0; // Disconnect external Low Frequency oscillator

  return EXT_CLK;
}

/*-----------------------------------------*/

/* Set CLK pin as output and disable others oscillators. Pulses must be done by
 * software. In this mode give EXT CLK just one pulse. Return oscillator type
 */
uint8_t Try_external_CLK_1_pulse(void)
{
  // Set CLK pin as output
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_out );

  EXT_XTAL_0;   // Disconnect external XTAL
  EXT_RC_0;     // Disconnect external RC oscillator
  EXT_LF_RES_0; // Disconnect external Low Frequency oscillator

  return EXT_CLK_1_pulse;
}
/*-----------------------------------------*/

/* Disable all oscillators and enable only Low Frequency oscillator, that must
 * be software emulated. Return oscillator type
 */
uint8_t Try_external_LF_RES(void)
{
  // Set CLK pin as input
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_in  );

  EXT_XTAL_0;   // Disconnect external XTAL
  EXT_RC_0;     // Disconnect external RC oscillator
  EXT_LF_RES_1; // Connect external Low Frequency oscillator

  return EXT_LF_OSC;
}

/*-----------------------------------------*/
