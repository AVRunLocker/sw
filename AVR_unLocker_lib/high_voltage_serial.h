/**
 * \file
 *
 * \brief Routines for unlocking in high voltage serial programming mode
 *
 * Created  21.06.2014\n
 * Modified 28.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#ifndef _HIGH_VOLTAGE_SERIAL_H_
#define _HIGH_VOLTAGE_SERIAL_H_
//================================| Includes |=================================
// Define integer types
#include <inttypes.h>

// Define common structures (such as signature_bytes_t)
#include "AVR_unLocker.h"

// Main variables for this project
#include "settings.h"

//===============================| Definitions |===============================
/* Name for easy high-voltage serial programming. Just link to existing pins,
 * but name them differently.
 */


#define SDI_PORT        D0_PORT
#define SDI_PIN         D0_PIN
#define SDI_0           D0_0
#define SDI_1           D0_1

#define SII_PORT        D1_PORT
#define SII_PIN         D1_PIN
#define SII_0           D1_0
#define SII_1           D1_1

#define SDO_PORT        D2_PORT
#define SDO_PIN         D2_PIN
#define SDO_0           D2_0
#define SDO_1           D2_1

/**
 * \brief Abstract time delay when writing fuse
 *
 * Do not change if you do not know exactly what are you doing! This is not\n
 * time in ms or us, so watch out.
 */
#define FUSE_WRITE_DELAY        150000

//===========================| Function prototypes |===========================

/**
 * \brief Try to communicate with target chip over high voltage serial
 * programming mode.
 *
 * If device respond properly, then all memory is erased and loaded default\n
 * fuses
 *
 * @return status code
 */
uint8_t Try_High_voltage_serial( void );


/**
 * \brief Try to load signature bits over parallel high voltage programming.
 *
 * @param p_signature_bytes Pointer to signature bytes
 * @param osc_type Oscillator type
 *
 * @return status code, where E_OK means "everything is OK", else there is\n
 * some problem (for example: "bad" oscillator).
 */
uint8_t HV_serial_load_signature_bits( signature_bytes_t *p_signature_bytes,
                                         uint8_t           osc_type);


/**
 * \brief Read fuses
 *
 * @param p_i_high Pointer to variable. To this address will be written high
 *  fuse Byte.
 * @param p_i_low Pointer to variable. To this address will be written low fuse
 *  Byte.
 * @param osc_type Oscillator type
 * @return status code (E_OK means OK)
 */
uint8_t HV_serial_read_fuses(uint8_t *p_i_high, uint8_t *p_i_low,
                             uint8_t osc_type);


/**
 * \brief Just try to set fuses
 *
 * @param i_high Fuse high Byte
 * @param i_low Fuse low Byte
 * @param osc_type Oscillator type
 * @return status code (E_OK means OK)
 */
uint8_t HV_serial_set_fuses(uint8_t i_high, uint8_t i_low,
                            uint8_t osc_type);


/**
 * \brief Send command which erase whole chip
 *
 * Erase cause deleting lock bits
 * @param osc_type Oscillator type
 * @return status code (E_OK means all OK)
 */
uint8_t HV_serial_erase_chip(uint8_t osc_type);



/**
 * \brief Simple function that only send and receive 1 Byte
 *
 * @param i_tx_SDI_Byte Data in Byte
 * @param i_tx_SII_Byte Instruction in Byte
 * @param p_rx_SDO_Byte Pointer to data out Byte
 * @param osc_type Oscillator type
 * @return status code (E_OK means all OK)
 */
uint8_t HV_serial_send_receive_Byte(uint8_t i_tx_SDI_Byte,
                                    uint8_t i_tx_SII_Byte,
                                    uint8_t *p_rx_SDO_Byte,
                                    uint8_t osc_type);
#endif
