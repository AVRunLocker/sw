/**
 * \file
 *
 * \brief Routines for unlocking in high voltage parallel programming mode
 *
 * Created  21.06.2014\n
 * Modified 04.09.2018
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#include "high_voltage_paralel.h"

//================================| Functions |================================


/**
 * \brief Try to communicate with target chip over high voltage programming
 * mode.
 *
 * If device respond properly, then all memory is erased and loaded default\n
 * fuses
 *
 * @return status code
 */
uint8_t Try_High_voltage_parallel( void )
{
  /* Default error code - do not know, where is error now, and process is not
   * done, so there is should be some error code :)
   */
  uint8_t i_status = E_unknown;


  // Inform user
  LCD1602_Clear_display();
#if version == debug
  LCD1602_write_text_from_flash( MSG_TRY_HV_PARALLEL );
#else
  LCD1602_write_text_from_flash_slow( MSG_TRY_HV_PARALLEL, text_delay );
#endif


  /* To this variable will be saved loaded signature from target (if any).
   * This variable is cleaned every time in function "SPI_load_signature_bits",
   * so it do not have to be "cleaned"
   */
  signature_bytes_t i_signature_bytes;


  /* By experiment was proved, that in high voltage parallel programming works
   * only with external CLK as source oscillator, no matter what oscillator is
   * set on target chip. So, that makes software little bit easier.
   */
  uint8_t osc_type = Try_external_CLK();


  /* Give function "HV_parallel_load_signature_bits" address, where to save
   * signature bytes. Second parameter is oscillator type. Return error code
   * (E_OK if all right).
   */
  i_status = HV_parallel_load_signature_bits( &i_signature_bytes, osc_type );
  if ( i_status != E_OK )
  {     // If not OK -> return error code
    DCDC_set_Vcc();     // Turn off DC/DC
    return i_status;
  }
  // Else continue

  // Try to find chip in database


  // There will be saved default fuses and chip name
  chip_attributes_t chip_attr;
  chip_attr.efuse = 0xFF;
  chip_attr.hfuse = 0xD9;
  chip_attr.lfuse = 0xE1;

  for ( int i=0 ; i<chip_name_max_length ; i++)
  {
    chip_attr.name[i] = 6+i;
  }

  // Fill "chip_attr" - try to find chip in database thru signature
  i_status = find_chip_in_database(
      i_signature_bytes.i_signature0,
      i_signature_bytes.i_signature1,
      i_signature_bytes.i_signature2,
      &chip_attr);

  LCD1602_Clear_display();
  // Test for results
  if ( i_status == E_chip_not_found_in_database )
  {     // If NOT found...
    DCDC_set_Vcc();     // Turn off DC/DC

#if version == debug
  LCD1602_write_text_from_flash( MSG_NOT_FND_IN_DTBSE_DBG );
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature0);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature1);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature2);
#else
    LCD1602_write_text_from_flash_slow( MSG_NOT_FND_IN_DTBSE, text_delay );
    _delay_ms( user_read_info_delay );  // Take user some time to read this MSG
    LCD1602_Clear_display();
    LCD1602_write_text_from_flash_slow( MSG_SIGNATURE, text_delay );
    LCD1602_write_hex_8bit(i_signature_bytes.i_signature0);
    LCD1602_write_hex_8bit(i_signature_bytes.i_signature1);
    LCD1602_write_hex_8bit(i_signature_bytes.i_signature2);
#endif
    _delay_ms( user_read_info_delay );  // Take user some time to read this MSG
    return i_status;
  }

  // Else chip is found, so we can write default fuses and Write chip name
#if version == debug
  LCD1602_write_text( chip_attr.name );
#else
  LCD1602_write_text_slow( chip_attr.name , text_delay );
#endif

  // OK, now try write default fuses

  // L fuse first
  // Wait for target
  i_status = HV_parallel_wait_until_ready( osc_type );
  if ( i_status != E_OK )
  {     // If problem (timeout) -> return
    DCDC_set_Vcc();     // Turn off DC/DC
    return i_status;
  }

  HV_parallel_Flash_prog_A( 0b01000000, osc_type );
  HV_parallel_Flash_prog_C( chip_attr.lfuse, osc_type );
  BS1_0;
  BS2_0;
  WR_0;
  _delay_ms( 1 );
  WR_1;

  HV_parallel_wait_until_ready( osc_type );
  // Wait for target
  i_status = HV_parallel_wait_until_ready( osc_type );
  if ( i_status != E_OK )
  {     // If problem (timeout) -> return
    DCDC_set_Vcc();     // Turn off DC/DC
    return i_status;
  }


  // H fuse next
  HV_parallel_Flash_prog_A( 0b01000000 , osc_type );
  HV_parallel_Flash_prog_C( chip_attr.hfuse, osc_type );
  BS1_1;
  BS2_0;
  WR_0;
  _delay_ms( 1 );
  WR_1;

  // Wait for target
  i_status = HV_parallel_wait_until_ready( osc_type );
  if ( i_status != E_OK )
  {     // If problem (timeout) -> return
    DCDC_set_Vcc();     // Turn off DC/DC
    return i_status;
  }


  // Ext fuse - just for case
  HV_parallel_Flash_prog_A( 0b01000000, osc_type );
  HV_parallel_Flash_prog_C( chip_attr.efuse, osc_type );
  BS1_0;
  BS2_1;
  WR_0;
  _delay_ms( 1 );
  WR_1;

  // Wait for target
  i_status = HV_parallel_wait_until_ready( osc_type );
  if ( i_status != E_OK )
  {     // If problem (timeout) -> return
    DCDC_set_Vcc();     // Turn off DC/DC
    return i_status;
  }


  // Turn off DC/DC
  DCDC_set_Vcc();

  LCD1602_set_address( 0x40 );
  LCD1602_write_text_from_flash_slow( MSG_UNLOCKED, text_delay );

  // Set status
  i_status = E_OK;

  return i_status;
}

/*-----------------------------------------*/

/**
 * \brief Try to load signature bits over parallel high voltage programming.
 *
 * @param p_signature_bytes Pointer to signature bytes
 * @param osc_type Oscillator type
 *
 * @return status code, where E_OK means "everything is OK", else there is\n
 * some problem (for example: "bad" oscillator).
 */
uint8_t HV_parallel_load_signature_bits( signature_bytes_t *p_signature_bytes,
                                         uint8_t           osc_type)
{
  // Again, first define error code for any case
  uint8_t i_status = E_unknown;

  // Set DC/DC out to Vcc -> +12V is needed later
  DCDC_set_Vcc();

  /* RST_TARGET set to 0 -> transistor turn off -> RST on target will be high
   * -> can not reprogram now
   */
  RST_TARGET_0;

  // Erase signature bytes
  p_signature_bytes->i_signature0 = 0x00;
  p_signature_bytes->i_signature1 = 0x00;
  p_signature_bytes->i_signature2 = 0x00;


  // Set data port as input
  set_io_data_in();
  // Set WR, OE, BS2 on default values
  WR_1;
  OE_1;
  BS2_0;

  // Set "Prog_enable" pins to "0000"
  PAGEL_0;
  XA1_0;
  XA0_0;
  BS1_0;

  // Turn on transistor on "Reset" pin -> reset on target is in low
  RST_TARGET_1;


  /* Set +12V on DC/DC - due to transistor voltage on target reset will be still
   * 0V and DC/DC will be prepared
   */
  DCDC_set_12V();

  // In AVR documentation is written, that target should get at least 6 pulses
  for ( int i=0 ; i<20 ; i++ )
  {
    give_CLK_pulse( osc_type );
  }


  // Small delay
  _delay_ms(1);

  // Get +12V on target reset -> turn off transistor
  RST_TARGET_0;

  // Small delay
  _delay_ms(1);


  // Try erase chip. Due to lock bits can be impossible read signature bits
  XA1_1;
  XA0_0;
  BS1_0;
  HV_parallel_send_byte( 0x80, osc_type );      // Command for erase

  // Write pulse
  WR_0;
  _delay_ms( 1 );
  WR_1;

  i_status = HV_parallel_wait_until_ready( osc_type );


  // Test for status
  if ( i_status != E_OK )
  {     // If something gets wrong, then return status code now
    return i_status;
  }
  // Else continue


  // Read signature0 byte
  // Command "Read Signature Bytes and Calibration byte"
  HV_parallel_Flash_prog_A( 0b00001000, osc_type );
  // Address 0x00 -> LSB -> signature0
  HV_parallel_Flash_prog_B( 0x00, osc_type );

  BS1_0;
  // Now read data on bus (function ...receive... set OE as needed)
  p_signature_bytes->i_signature0 = HV_parallel_receive_byte( osc_type );

  /* Test signature for non-valid values (0x00 and 0xFF -> something is
   * probably wrong)
   */
  if ( (p_signature_bytes->i_signature0)==0x00 )
  {
    i_status = E_no_response;
    return i_status;
  }
  if ( (p_signature_bytes->i_signature0)==0xFF )
  {
    i_status = E_incorrect_response;
    return i_status;
  }

  // Else we can read signature1 byte. Analogy with reading signature0
  HV_parallel_Flash_prog_A( 0b00001000, osc_type );
  HV_parallel_Flash_prog_B( 0x01, osc_type );
  BS1_0;
  p_signature_bytes->i_signature1 = HV_parallel_receive_byte( osc_type );

  // Now there shouldn't be non-valid values, so test is skipped

  // And, finally, read signature byte 2
  HV_parallel_Flash_prog_A( 0b00001000, osc_type );
  HV_parallel_Flash_prog_B( 0x02, osc_type );
  BS1_0;
  p_signature_bytes->i_signature2 = HV_parallel_receive_byte( osc_type );

  // And set status to OK
  i_status = E_OK;
  return i_status;
}

/*-----------------------------------------*/

/**
 * \brief Just send 8 bits over parallel bus when high voltage mode is used.
 * @param data Data to send.
 * @param osc_type Oscillator type
 */
void HV_parallel_send_byte( uint8_t data, uint8_t osc_type )
{
  // Disable OE (Just for case)
  OE_1;

  // Set data port as output (All outputs to 0)
  set_io_data_out();


  // Test for MSB bit -> use masking with function logic AND
  if ( (data & 0b10000000) != 0 )       // If 1 -> set output as non zero.
  {
    D7_1;       // Then set D7 to 1, else stay 0
  }

  // Test another bit...
  if ( (data & 0b01000000) != 0 )
  {
    D6_1;
  }
  // And another...
  if ( (data & 0b00100000) != 0 )
  {
    D5_1;
  }
  if ( (data & 0b00010000) != 0 )
  {
    D4_1;
  }
  if ( (data & 0b00001000) != 0 )
  {
    D3_1;
  }
  if ( (data & 0b00000100) != 0 )
  {
    D2_1;
  }
  if ( (data & 0b00000010) != 0 )
  {
    D1_1;
  }
  if ( (data & 0b00000001) != 0 )
  {
    D0_1;
  }

  // Give pulse
  give_CLK_pulse( osc_type );

  // Set direction of data bus to input
  set_io_data_in();
}

/*-----------------------------------------*/

/**
 * \brief Receive data from target chip (in high voltage parallel mode)
 *
 * @param osc_type Oscillator type
 * @return  Received data from target chip
 */
uint8_t HV_parallel_receive_byte( uint8_t osc_type)
{
  // Set direction of data bus to input
  set_io_data_in();

  // Enable OE
  OE_0;

  // To this variable will be saved input data
  uint8_t data = 0x00;


  // Now read data on bus, bit by bit
  data = data | (read_in_pin( &D7_PORT, D7_PIN ))<<7;
  data = data | (read_in_pin( &D6_PORT, D6_PIN ))<<6;
  data = data | (read_in_pin( &D5_PORT, D5_PIN ))<<5;
  data = data | (read_in_pin( &D4_PORT, D4_PIN ))<<4;
  data = data | (read_in_pin( &D3_PORT, D3_PIN ))<<3;
  data = data | (read_in_pin( &D2_PORT, D2_PIN ))<<2;
  data = data | (read_in_pin( &D1_PORT, D1_PIN ))<<1;
  data = data | (read_in_pin( &D0_PORT, D0_PIN ))<<0;

  // Data saved. Disable OE
  OE_1;

  return data;
}

/*-----------------------------------------*/

/**
 * \brief Wait for target chip.
 *
 * When target is not ready to time defined in definition
 * "HV_programming_wait_for_RDY" (in ms), the return error code, which inform,
 * that target not response. This could points to wrong type oscillator.
 *
 * @param osc_type Oscillator type
 * @return Status code.
 */
uint8_t HV_parallel_wait_until_ready( uint8_t osc_type )
{
  // Again, set unknown error code
  uint8_t i_status = E_unknown;

  // Define counter (counts time, in witch waiting for target)
  uint8_t cnt;

  // Decrease until "cnt" reach 255 (zero minus one is 255)
  for ( cnt=HV_programming_wait_for_RDY ; cnt > 0 ; cnt-- )
  {
    // Read RDY/BSY flag and test it!
    if ( ( read_in_pin( &RDY_PORT, RDY_PIN ) ) == 1 )
    {// If 1 -> target ready -> OK
      // Set status
      i_status = E_OK;
      // And exit
      return i_status;
    }
    else
    {/* Set status on "E_BSY_time_out" -> when for end, then in variable
      * "i_status" will be saved this error code
      */
      i_status = E_BSY_time_out;

      // And wait some time ( give some pulses )
      give_CLK_pulse( osc_type );
    }
  }

  // All done. Return status
  return i_status;
}


/*-----------------------------------------*/

/* Due to efficient programming it is useful create functions, that makes some
 * whole commands. So, there are some....
 */

/**
 * \brief Command "A" for programming Flash (load command)
 * @param command Command byte (8 bits)
 * @param osc_type Oscillator type
 */
void HV_parallel_Flash_prog_A( uint8_t command, uint8_t osc_type )
{
  XA1_1;
  XA0_0;
  BS1_0;
  HV_parallel_send_byte( command, osc_type );
}

/**
 * \brief Command "B" for programming Flash (load address low byte)
 * @param addr_low Address low byte
 * @param osc_type Oscillator type
 */
void HV_parallel_Flash_prog_B( uint8_t addr_low, uint8_t osc_type )
{
  XA1_0;
  XA0_0;
  BS1_0;
  HV_parallel_send_byte( addr_low, osc_type);
}

/**
 * \brief Command "C" for programming Flash (load data low byte)
 * @param data_low Data low byte
 * @param osc_type Oscillator type
 */
void HV_parallel_Flash_prog_C( uint8_t data_low, uint8_t osc_type )
{
  XA1_0;
  XA0_1;
  HV_parallel_send_byte( data_low, osc_type);
}

/*-----------------------------------------*/
