/**
 * \brief This is chip database for AVR unLocker
 *
 * ...but it can be use for any other projects too :)\n
 * Database contains: chip signature, name, hfuse, lfuse, efuse
 *
 * Created:  2012.10.25\n
 * Modified: 2016.08.09
 *
 * \author Martin Stejskal - martin.stej@gmail.com
 */


#include "chip_database.h"


//===========================| Great AVR database |============================

/**
 * \brief Database as 8 bit 2 dimensional array.
 *
 * Format: signature0, signature1, signature2, low fuse, high fuse, extended\n
 * fuse, 16 characters for name -> in sum 22 bytes
 */
const char chip_database[][6+chip_name_max_length] PROGMEM =
    {
        // ATmega8 and ATmega8A
        {0x1E, 0x93, 0x07, 0xE1, 0xD9, 0xFF,
            'A','T','m','e','g','a','8',NULL},

        // ATmega16, ATmega16A
        {0x1E, 0x94, 0x03, 0xE1, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6',NULL},

        // ATmega32, ATmega32A
        {0x1E, 0x95, 0x02, 0xE1, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2',NULL},

        // ATmega48, ATmega48A
        {0x1E, 0x92, 0x05, 0x62, 0xDF, 0xFF,
            'A','T','m','e','g','a','4','8',NULL},

        // ATmega48PA
        {0x1E, 0x92, 0x0A, 0x62, 0xDF, 0xFF,
            'A','T','m','e','g','a','4','8','P','A',NULL},

        // ATmega64, ATmega64A
        {0x1E, 0x96, 0x02, 0xE1, 0x99, 0xFD,
            'A','T','m','e','g','a','6','4',NULL},

        // ATmega88, ATmega88A
        {0x1E, 0x93, 0x0A, 0x62, 0xDF, 0xF9,
            'A','T','m','e','g','a','8','8',NULL},

        // ATmega88PA
        {0x1E, 0x93, 0x0F, 0x62, 0xDF, 0xF9,
            'A','T','m','e','g','a','8','8','P','A',NULL},

        // ATmega103
        {0x1E, 0x97, 0x01, 0xDF, 0xFF, 0xFF,
            'A','T','m','e','g','a','1','0','3',NULL},

        // ATmega128, ATmega128A
        {0x1E, 0x97, 0x02, 0xE1, 0x99, 0xFD,
            'A','T','m','e','g','a','1','2','8',NULL},

        // ATmega161
        {0x1E, 0x94, 0x01, 0xDA, 0xFF, 0xFF,
            'A','T','m','e','g','a','1','2','8',NULL},

        // ATmega162
        {0x1E, 0x94, 0x04, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6','2',NULL},

        // ATmega163
        {0x1E, 0x94, 0x02, 0xD2, 0xFF, 0xFF,
            'A','T','m','e','g','a','1','6','3',NULL},

        // ATmega165
        {0x1E, 0x94, 0x05, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6','5',NULL},

        // ATmega165A
        {0x1E, 0x94, 0x10, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6','5','A',NULL},

        // ATmega165PA
        {0x1E, 0x94, 0x07, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6','5','P','A',NULL},

        // ATmega168, ATmega168A
        {0x1E, 0x94, 0x06, 0x62, 0xDF, 0xF9,
            'A','T','m','e','g','a','1','6','8',NULL},

        // ATmega168PA
        {0x1E, 0x94, 0x0B, 0x62, 0xDF, 0xF9,
            'A','T','m','e','g','a','1','6','8','P','A',NULL},

        // ATmega169, ATmega169PA
        {0x1E, 0x94, 0x05, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6','9',NULL},

        // ATmega169A
        {0x1E, 0x94, 0x11, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','6','9','A',NULL},

        // ATmega323
        {0x1E, 0x95, 0x01, 0xF2, 0x9F, 0xFF,
            'A','T','m','e','g','a','3','2','3',NULL},

        // ATmega324A
        {0x1E, 0x95, 0x15, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','4','A',NULL},

        // ATmega324PA
        {0x1E, 0x95, 0x11, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','4','P','A',NULL},

        // ATmega325, ATmega325A
        {0x1E, 0x95, 0x05, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','5',NULL},

        // ATmega325PA
        {0x1E, 0x95, 0x0D, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','5','P','A',NULL},

        // ATmega328
        {0x1E, 0x95, 0x14, 0x62, 0xD9, 0xFF,
            'A','T','m','e','g','a','3','2','8',NULL},

        // ATmega328P
        {0x1E, 0x95, 0x0F, 0x62, 0xD9, 0xFF,
            'A','T','m','e','g','a','3','2','8','P',NULL},
        
        // ATmega329
        {0x1E, 0x95, 0x03, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','9',NULL},

        // ATmega329P
        {0x1E, 0x95, 0x0B, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','9','P',NULL},

        // ATmega406
        {0x1E, 0x95, 0x07, 0xCD, 0xFE, 0xFF,
            'A','T','m','e','g','a','4','0','6',NULL},

        // ATmega640
        {0x1E, 0x96, 0x08, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','0',NULL},

        // ATmega644, ATmega644A
        {0x1E, 0x96, 0x09, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','4',NULL},

        // ATmega644PA
        {0x1E, 0x96, 0x0A, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','4','P','A',NULL},

        // ATmega645, ATmega645A
        {0x1E, 0x96, 0x05, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','5',NULL},

        // ATmega645P
        {0x1E, 0x96, 0x0D, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','5','P',NULL},

        // ATmega649, ATmega649A
        {0x1E, 0x96, 0x03, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','9',NULL},

        // ATmega649P
        {0x1E, 0x96, 0x0B, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','9','P',NULL},

        // ATmega1280
        {0x1E, 0x97, 0x03, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','2','8','0',NULL},

        // ATmega1281
        {0x1E, 0x97, 0x04, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','2','8','1',NULL},

        // ATmega1284
        {0x1E, 0x97, 0x06, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','2','8','4',NULL},

        // ATmega1284P
        {0x1E, 0x97, 0x05, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','1','2','8','4','P',NULL},

        // ATmega2560
        {0x1E, 0x98, 0x01, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','2','5','6','0',NULL},

        // ATmega2561
        {0x1E, 0x98, 0x02, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','2','5','6','1',NULL},

        // ATmega3250, ATmega3250A
        {0x1E, 0x95, 0x06, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','5','0',NULL},

        // ATmega3250PA
        {0x1E, 0x95, 0x0E, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','5','0','P','A',NULL},

        // ATmega3290, ATmega3290A
        {0x1E, 0x95, 0x04, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','9','0',NULL},

        // ATmega3290PA
        {0x1E, 0x95, 0x0C, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','3','2','9','0','P','A',NULL},

        // ATmega6450, ATmega6450A
        {0x1E, 0x96, 0x06, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','5','0',NULL},

        // ATmega6450P
        {0x1E, 0x96, 0x0E, 0x62, 0x99, 0xFF,
            'A','T','m','e','g','a','6','4','5','0','P',NULL},

        // ATmega8515
        {0x1E, 0x93, 0x06, 0xE1, 0xD9, 0xFF,
            'A','T','m','e','g','a','8','5','1','5',NULL},

        // ATmega8535
        {0x1E, 0x93, 0x08, 0xE1, 0xD9, 0xFF,
            'A','T','m','e','g','a','8','5','3','5',NULL},


        // ATtiny13
        {0x1E, 0x90, 0x07, 0x6A, 0xFF, 0xFF,
            'A','T','t','i','n','y','1','3',NULL},

        // ATtiny15
        {0x1E, 0x90, 0x06, 0xFF, 0xFF, 0xFF,
            'A','T','t','i','n','y','1','5',NULL},

        // ATtiny22
        {0x1E, 0x91, 0x06, 0xFF, 0xFF, 0xFF,
            'A','T','t','i','n','y','2','2',NULL},
    
        // ATtiny24
        {0x1E, 0x91, 0x0B, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','2','4',NULL},    

        // ATtiny25
        {0x1E, 0x91, 0x08, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','2','5',NULL},

        // ATtiny26
        {0x1E, 0x91, 0x09, 0xE1, 0xF7, 0xFF,
            'A','T','t','i','n','y','2','6',NULL},    
    
        // ATtiny28
        {0x1E, 0x91, 0x07, 0xFF, 0xFF, 0xFF,
            'A','T','t','i','n','y','2','8',NULL},

        // ATtiny40
        {0x1E, 0x92, 0x0E, 0xFF, 0xFF, 0xFF,
            'A','T','t','i','n','y','4','0',NULL},    

        // ATtiny43
        {0x1E, 0x92, 0x0C, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','4','3',NULL},

        // ATtiny44
        {0x1E, 0x92, 0x07, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','4','4',NULL},    

        // ATtiny45
        {0x1E, 0x92, 0x06, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','4','5',NULL},

        // ATtiny48
        {0x1E, 0x92, 0x09, 0x6E, 0xDF, 0xFF,
            'A','T','t','i','n','y','4','8',NULL},

        // ATtiny84
        {0x1E, 0x93, 0x0C, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','8','4',NULL},   
    
        // ATtiny85
        {0x1E, 0x93, 0x0B, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','8','5',NULL},

        // ATtiny87
        {0x1E, 0x93, 0x87, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','8','7',NULL},

        // ATtiny88
        {0x1E, 0x93, 0x11, 0x6E, 0xDF, 0xFF,
            'A','T','t','i','n','y','8','8',NULL},

        // ATtiny167
        {0x1E, 0x94, 0x87, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','1','5',NULL},

        // ATtiny261
        {0x1E, 0x91, 0x0C, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','2','6','1',NULL},

        // ATtiny461
        {0x1E, 0x92, 0x08, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','4','6','1',NULL},

        // ATtiny861
        {0x1E, 0x93, 0x0D, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','8','6','1',NULL},

        // ATtiny2313
        {0x1E, 0x91, 0x0A, 0x64, 0xDF, 0xFF,
            'A','T','t','i','n','y','2','3','1','3',NULL},

        // ATtiny4313
        {0x1E, 0x92, 0x0D, 0x62, 0xDF, 0xFF,
            'A','T','t','i','n','y','4','3','1','3',NULL},

        /* Code for end of database (must not be same as any of signature
         * before!!!). In hex is 'E''N''D' represented by following values:
         * 0x45, 0x4E, 0x44
         */
        {'E','N','D',NULL}
    };



//================================| Functions |================================

/**
 * \brief Try to find chip in database through signature.
 *
 * @param i_signature0 Signature byte 0 (LSB)
 * @param i_signature1 Signature byte 1
 * @param i_signature2 Signature byte 2 (MSB)
 * @param p_chip_attr Pointer to structure where data will be saved. It is\n
 * a little bit complicated to return whole structure in C, so let's make it\n
 * the easy way.
 *
 * @return Status which inform if chip was found or not
 */
uint8_t find_chip_in_database(
    char i_signature0, char i_signature1, char i_signature2,
    chip_attributes_t *chip_attr )
{
  // To structure "chip_attr" will be saved result fuses and name

  // First clean "chip_attr"
  chip_attr->lfuse=0x00;
  chip_attr->hfuse=0x00;
  chip_attr->efuse=0x00;
  uint8_t i_status = E_chip_not_found_in_database;  // Not found yet
  // And clean "chip_attr.name"
  for (uint8_t i=0 ; i<chip_name_max_length ; i++)
  {
    chip_attr->name[i]='?';           // And store symbol in brackets
  }


  // Variable for loading data from flash and do comparing and so on
  char data0, data1, data2;

  // Counter for database position (specify chip index). 255 should be enough
  uint8_t cnt=0;        // Start on 0

  // Searching is done in cycle
  while( i_status == E_chip_not_found_in_database )
  {
    // to "data0" load first byte in database (signature0 position)
    data0 = pgm_read_byte(&(chip_database[cnt][0]));
    // Same, but for second byte in database
    data1 = pgm_read_byte(&(chip_database[cnt][1]));
    // Again, but for third byte in database
    data2 = pgm_read_byte(&(chip_database[cnt][2]));

    // Test if it is not end of database -> not found -> return not found
    if ( (data0 == 'E') & (data1 == 'N') & (data2 == 'D') )
    {
      // "chip_attr.status" is already set to "not found"
      // "i_status" is already set to "E_chip_not_found_in_database"
      return i_status;
    }

    // Else compare signature bits with given signature. If equal -> chip found
    if (
    (data0 == i_signature0) & (data1 == i_signature1) & (data2 == i_signature2)
       )
    {
      // OK, chip found... let's fill "chip_attr"

      // Default fuses
      chip_attr->lfuse = pgm_read_byte(&(chip_database[cnt][3]));
      chip_attr->hfuse = pgm_read_byte(&(chip_database[cnt][4]));
      chip_attr->efuse = pgm_read_byte(&(chip_database[cnt][5]));

      // Name
      uint8_t name_cnt=6;       /* Calculate index for array of chars (string)-
                                 * Starts on 6 -> in database name starts on
                                 * index 6
                                 */
      /* Load characters until NULL is found (Yes, we can make
       * "chip_name_max_length" steps, but it is not so effective for short
       * names. So, it is depends)
       */
      while ( name_cnt <= chip_name_max_length )        /* Condition is just
                                                         * for case
                                                         */
      {
        chip_attr->name[name_cnt-6] =
            pgm_read_byte(&(chip_database[cnt][name_cnt]));

        // Test for null symbol
        if ( chip_attr->name[name_cnt-6] == NULL )
        {       // If found -> break cycle
          break;
        }

        name_cnt++;
      }

      // And set status
      i_status = E_chip_found_in_database;
    }
    else        // Else chip not found yet in database -> increase counter
    {
      cnt++;
    }
  }

  // Return just status
  return i_status;
}

