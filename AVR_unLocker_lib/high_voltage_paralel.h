/**
 * \file
 *
 * \brief Routines for unlocking in high voltage parallel programming mode
 *
 * Created  21.06.2014\n
 * Modified 21.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#ifndef _HIGH_VOLTAGE_PARALEL_H_
#define _HIGH_VOLTAGE_PARALEL_H_

//================================| Includes |=================================
// Define integer types
#include <inttypes.h>

// Define common structures (such as signature_bytes_t)
#include "AVR_unLocker.h"


//===========================| Function prototypes |===========================

/**
 * \brief Try to communicate with target chip over high voltage programming
 * mode.
 *
 * If device respond properly, then all memory is erased and loaded default\n
 * fuses
 *
 * @return status code
 */
uint8_t Try_High_voltage_parallel( void );


/**
 * \brief Try to load signature bits over parallel high voltage programming.
 *
 * @param p_signature_bytes Pointer to signature bytes
 * @param osc_type Oscillator type
 *
 * @return status code, where E_OK means "everything is OK", else there is\n
 * some problem (for example: "bad" oscillator).
 */
uint8_t HV_parallel_load_signature_bits( signature_bytes_t *p_signature_bytes,
                                         uint8_t           osc_type);



/**
 * \brief Just send 8 bits over parallel bus when high voltage mode is used.
 * @param data Data to send.
 * @param osc_type Oscillator type
 */
void HV_parallel_send_byte( uint8_t data, uint8_t osc_type );

/**
 * \brief Receive data from target chip (in high voltage parallel mode)
 *
 * @param osc_type Oscillator type
 * @return  Received data from target chip
 */
uint8_t HV_parallel_receive_byte( uint8_t osc_type);

/**
 * \brief Wait for target chip.
 *
 * When target is not ready to time defined in definition
 * "HV_programming_wait_for_RDY" (in ms), the return error code, which inform,
 * that target not response. This could points to wrong type oscillator.
 *
 * @param osc_type Oscillator type
 * @return Status code.
 */
uint8_t HV_parallel_wait_until_ready( uint8_t osc_type );


/* Due to efficient programming it is useful create functions, that makes some
 * whole commands. So, there are some....
 */

/**
 * \brief Command "A" for programming Flash (load command)
 * @param command Command byte (8 bits)
 * @param osc_type Oscillator type
 */
void HV_parallel_Flash_prog_A( uint8_t command, uint8_t osc_type );

/**
 * \brief Command "B" for programming Flash (load address low byte)
 * @param addr_low Address low byte
 * @param osc_type Oscillator type
 */
void HV_parallel_Flash_prog_B( uint8_t addr_low, uint8_t osc_type );

/**
 * \brief Command "C" for programming Flash (load data low byte)
 * @param data_low Data low byte
 * @param osc_type Oscillator type
 */
void HV_parallel_Flash_prog_C( uint8_t data_low, uint8_t osc_type );

#endif
