/* This is library for communication with 1602 (16x2 characters) LCDs.
 * It is common used LCD. This library use 4 data connection, witch reduce
 * number of used pins to minimum.
 *
 * Usage: #include "relative_path_to_this_file/LCD_1602.c"
 *  When include C file, You have not configure compiler or project properties
 *
 * Martin Stejskal - martin.stej@gmail.com
 */

#ifndef _LCD_1602_library       // If not defined then....
#define _LCD_1602_library

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/io.h>             // Inputs/outputs
#include <inttypes.h>           // Define integer types
#include <avr/pgmspace.h>       /* For work with memory. There is used for save
                                 * text to flash, because these texts are not
                                 * changing.
                                 */

#include "io_functions.h"       // Basic functions with PORTs

#include "settings.h"           // Just because F_CPU variable

// Because of _delay_* functions -> now should be constants
#define __DELAY_BACKWARD_COMPATIBLE__
#include <util/delay.h>         // Delays
/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/

/* Assign names to real pins for easy use */
#define LCD_RS_PORT     PORTD   // Port name
#define LCD_RS_PIN      6       // Pin number. Can be assign as PA0, it is same

#define LCD_E_PORT      PORTA
#define LCD_E_PIN       0

#define LCD_D4_PORT     PORTD
#define LCD_D4_PIN      2

#define LCD_D5_PORT     PORTD
#define LCD_D5_PIN      3

#define LCD_D6_PORT     PORTD
#define LCD_D6_PIN      4

#define LCD_D7_PORT     PORTD
#define LCD_D7_PIN      5


/* Set default settings of LCD */

// Cursor settings - options: cursor_blink, cursor_not_blink, cursor_off
#define LCD_defaut_cursor cursor_blink

/* Following lines should not be changed (not needed) unless You really know
 * what are you doing
 */

/* Macros for setting pins output */
// Set E to 0
#define LCD_E_0         LCD_E_PORT = LCD_E_PORT & ~(1<<LCD_E_PIN)
// Set E to 1
#define LCD_E_1         LCD_E_PORT = LCD_E_PORT | (1<<LCD_E_PIN)

#define LCD_RS_0        LCD_RS_PORT = LCD_RS_PORT & ~(1<<LCD_RS_PIN)
#define LCD_RS_1        LCD_RS_PORT = LCD_RS_PORT | (1<<LCD_RS_PIN)


#define LCD_D4_0        LCD_D4_PORT = LCD_D4_PORT & ~(1<<LCD_D4_PIN)
#define LCD_D4_1        LCD_D4_PORT = LCD_D4_PORT | (1<<LCD_D4_PIN)

#define LCD_D5_0        LCD_D5_PORT = LCD_D5_PORT & ~(1<<LCD_D5_PIN)
#define LCD_D5_1        LCD_D5_PORT = LCD_D5_PORT | (1<<LCD_D5_PIN)

#define LCD_D6_0        LCD_D6_PORT = LCD_D6_PORT & ~(1<<LCD_D6_PIN)
#define LCD_D6_1        LCD_D6_PORT = LCD_D6_PORT | (1<<LCD_D6_PIN)

#define LCD_D7_0        LCD_D7_PORT = LCD_D7_PORT & ~(1<<LCD_D7_PIN)
#define LCD_D7_1        LCD_D7_PORT = LCD_D7_PORT | (1<<LCD_D7_PIN)

/* Definitions for default LCD settings */
#define cursor_off              0
#define cursor_not_blink        1
#define cursor_blink            2

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/

/* Prepare ports and initialize LCD, so next communication with LCD will be
 * easy. And clear display
 */
void LCD1602_init(void);

// Delete all symbols on LCD
void LCD1602_Clear_display(void);

// Write one symbol on LCD. Example: LCD1602_write_char('C'); // Write "C"
void LCD1602_write_char( uint8_t i_char );

// Write whole string on LCD. Example LCD1602_write_text("test text");
void LCD1602_write_text( char * s_text );

/* Write whole string on LCD, but insert time delay between single characters.
 * Example: LCD1602_write_text_slow( "Some text", 200);
 * Write string on LCD and between every symbol (character) insert time delay
 * 200 ms
 */
void LCD1602_write_text_slow( char * s_text , const int i_delay );

/* Very similar to "LCD1602_write_text", but instead of reading string
 * from SRAM it reads data from flash memory, witch is useful on embedded
 * systems with small SRAM.
 * Example: LCD1602_write_text_from_flash( string_variable );
 * Where string variable is declared this way:
 *  const char string_variable[] PROGMEM = "Some text to\n be written";
 */
void LCD1602_write_text_from_flash( const char * s_text_flash );

/* Again, similar to "LCD1602_write_text_slow", but instead of reading string
 * from SRAM it reads data from flash memory and give some time delay between
 * every single symbol. Delay is in ms.
 * Example: LCD1602_write_text_from_flash( string_variable, 100 );
 * Where string variable is declared this way:
 *  const char string_variable[] PROGMEM = "Some text to\n be written";
 * Between every symbol will be insert 100ms delay.
 */
void LCD1602_write_text_from_flash_slow(
    const char * s_text_flash , const int i_delay );

/* Write input 8 bit value as HEX number.
 * Example: LCD1602_write_hex_8bit( 170 );
 * Write AA to LCD (170 in dec is 0xAA)
 */
void LCD1602_write_hex_8bit( uint8_t i_data );

/* Write input 8 bit value as HEX number and insert between single characters
 * delay.
 * Example: LCD1602_write_hex_8bit( 170 , 35);
 * Write AA to LCD (170 in dec is 0xAA) with 35 ms delay
 */
void LCD1602_write_hex_8bit_slow( uint8_t i_data , int i_delay);

/* Set address on LCD. That means cursor position. "First" line has address
 * range from 0x00 to 0x27. The "second" line begins at 0x40 thru 0x67. If
 * invalid address given, then return non-zero value
 */
uint8_t LCD1602_set_address( uint8_t i_addr );

/* Input value - 0~F. Write as one digit to LCD.
 * Example: LCD1602_write_one_digit( 0xC );
 * Write to LCD symbol C
 */
void LCD1602_write_one_digit( uint8_t digit );

/* Send 8 data bits to LCD. Do no set RS signal, so programmer must set it
 * before this function is called.
 */
void LCD1602_send_8bits( uint8_t i_data );

// Low-level function. Just generate one enable pulse for LCD
void LCD1602_Signal_enable(void);

// Prepare pins, witch will be used for communication with LCD
void LCD1602_set_IO(void);


#endif

#ifndef NULL
  #define NULL (0)
#endif
