/* This library afford use every single pin on AVR. It is useful, when program
 * is almost done, but for hardware design is advantageous use different pin
 * (for example in case, when it simplify routing process). But as disadvantage
 * every function need some time to be done.
 *
 *
 * Martin Stejskal - martin.stej@gmail.com
 */

#include "io_functions.h"

/* Set pin as output, or input. This depends on b_birection (0 - in ; 1 - out).
 * Input parameters are: port address (example: &PORTA), pin number (example:
 * 0~7, or PA3), direction (0 - in ; 1 - out). Advantage this function is that
 * user just define PORTx and pin number and this function set DDRx registers.
 * If is pin set as output, then will be set to zero. If will be set as input,
 * then pull-up resistor will be disabled.
 * Example: set_pin( &PORTD, 3, 1);     // Set PD3 as output
 * Notice: MUST be used '&' ,because we need to get address, not port "variable"
 */
inline void set_pin(
    volatile uint8_t * p_port_addr ,    // If no volatile -> warning
    uint8_t i_pin_number ,
    uint8_t b_direction )
{
  volatile uint8_t * p_ddr_addr;
  /* OK, we have PORTx address, but we need DDRx address. However guys in Atmel
   * thought when designed AVR8 architecture, so in most AVR is PINx, DDRx and
   * PORTx address. So if we need DDRx we just decrement pointer value by 1.
   */
  p_ddr_addr = p_port_addr -1;


  if ( b_direction == dir_in)
  { // If input
    *p_ddr_addr = *p_ddr_addr & ~(1<<i_pin_number);
  }
  else  // Else it is output
  {
    *p_ddr_addr =  *p_ddr_addr | (1<<i_pin_number);    // Just make OR function
    *p_port_addr = *p_port_addr &(1<<i_pin_number);
    /* Little complicated. In assembler is easy write one bit, but not in
     * C language. So first is shifted logical one by i_pin_number to left,
     * so for example we got 0b00100000. Then is done bit negation -> we
     * get 0b11011111. And finally is done logical AND with original
     * register. So if register contained 0b01110000, then there will be
     * 0b01010000 (Simply on position i_pin_number will appear zero).
     */
  }
}

/*-----------------------------------------*/

/* Set output pin (must be set before) to logical value 0 or 1.
 * Example: set_out_pin( &PORTB, 3, 1); // Set PB3 to logic 1
 * Notice: MUST be used '&' ,because we need to get address, not port "variable"
 */

inline void set_out_pin(
    volatile uint8_t * p_port_addr ,    // If no volatile -> warning
    uint8_t i_pin_number ,
    uint8_t b_value )
{
  // Test for logic 0
  if ( b_value == 0 )
    {
      // Now function will work with value on witch pointer points
      * p_port_addr = * p_port_addr & ~(1<<i_pin_number);
    }
  // Else is not zero -> must be 1
  else
    {
      * p_port_addr = * p_port_addr | (1<<i_pin_number);
    }
}

/*-----------------------------------------*/

/* Read value on defined pin (must be set as input before). Return value is
 * value, that is read.
 * Example: pin_value = read_in_pin( &PORTC, 5);        // Read data from PC5
 * Notice: MUST be used '&' ,because we need to get address, not port "variable"
 */
inline uint8_t read_in_pin(
    volatile uint8_t * p_port_addr,     // If no volatile -> warning
    uint8_t i_pin_number )
{
  /* OK, we have PORTx address, but we need PINx address. However guys in Atmel
   * thought when designed AVR8 architecture, so in most AVR is PINx, DDRx and
   * PORTx address. So if we need PINx we just decrement pointer value by 2.
   */
  p_port_addr = p_port_addr-2;
  // Now in p_port_addr we have PINx address. Easy...


  /* Think! We have already address to memory (p_port_addr). So we know memory
   * address and bit position.
   * So we read from address (*p_port_addr) and do masking. So we get wanted
   * bit. There could be offset so if result will not be zero, there is 1.
   * Else there is 0.
   * It is more efficient use if not equal 0, because for AVR exist such
   * instruction (BRNE).
   */
  if((*p_port_addr & (1<<i_pin_number)) != 0)
  {
    return 1;
  }
  else
  {
    return 0;
  }
  // Program can not jump here.
}
