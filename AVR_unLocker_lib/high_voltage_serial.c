/**
 * \file
 *
 * \brief Routines for unlocking in high voltage serial programming mode
 *
 * Created  21.06.2014\n
 * Modified 28.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#include "high_voltage_serial.h"

//================================| Functions |================================

/**
 * \brief Try to communicate with target chip over high voltage serial
 * programming mode.
 *
 * If device respond properly, then all memory is erased and loaded default\n
 * fuses
 *
 * @return status code
 */
uint8_t Try_High_voltage_serial( void )
{
  /* Default error code - do not know, where is error now, and process is not
   * done, so there is should be some error code :)
   */
  uint8_t i_status = E_unknown;

  // Inform user
  LCD1602_Clear_display();
#if version == debug
  LCD1602_write_text_from_flash( MSG_TRY_HV_SERIAL );
#else
  LCD1602_write_text_from_flash_slow( MSG_TRY_HV_SERIAL, text_delay );
#endif


  /* To this variable will be saved loaded signature from target (if any).
   * This variable is cleaned every time in function "SPI_load_signature_bits",
   * so it do not have to be "cleaned"
   */
  signature_bytes_t i_signature_bytes;


  /* By experiment was proved, that in high voltage parallel programming works
   * only with external CLK as source oscillator, no matter what oscillator is
   * set on target chip. So, that makes software little bit easier.
   */
  uint8_t osc_type = Try_external_CLK_1_pulse();

  // Set DC/DC out to Vcc -> +12V is needed later
  DCDC_set_Vcc();

  /* RST_TARGET set to 0 -> transistor turn off -> RST on target will be high
   * -> can not reprogram now
   */
  RST_TARGET_0;

  // Set SDI, SDII and SDO to 0 (low level)
  set_pin(&SDI_PORT,    SDI_PIN,        dir_out);
  set_pin(&SII_PORT,    SII_PIN,        dir_out);
  set_pin(&SDO_PORT,    SDO_PIN,        dir_out);

  /* Set +12V on DC/DC - due to transistor voltage on target reset will be still
   * 0V and DC/DC will be prepared
   */
  DCDC_set_12V();



  // Erase signature bytes
  i_signature_bytes.i_signature0 = 0x00;
  i_signature_bytes.i_signature1 = 0x00;
  i_signature_bytes.i_signature2 = 0x00;

  // Set RESET to LOW -> enable transistor
  RST_TARGET_1;
  _delay_us(20);

  // Apply +12V do reset
  RST_TARGET_0;

  // Delay - at least 10us
  _delay_us(10);

  // release SDO (direction IN)
  set_pin(&SDO_PORT,    SDO_PIN,        dir_in);

  // Delay at least 300 us
  _delay_us(300);



  /* Give function "HV_parallel_load_signature_bits" address, where to save
   * signature bytes. Second parameter is oscillator type. Return error code
   * (E_OK if all right).
   */
  i_status = HV_serial_load_signature_bits( &i_signature_bytes, osc_type );
  if ( i_status != E_OK )
  {     // If not OK -> return error code
    return i_status;
  }

  // Now find same signature bits in database

  // There will be saved default fuses and chip name
  chip_attributes_t chip_attr;

  // Fill "chip_attr" - try to find chip in database thru signature
  i_status = find_chip_in_database(
      i_signature_bytes.i_signature0,
      i_signature_bytes.i_signature1,
      i_signature_bytes.i_signature2,
      &chip_attr);

  LCD1602_Clear_display();

  // Test for status - if not found...
  if ( i_status == E_chip_not_found_in_database )
  {
#if version == debug
  LCD1602_write_text_from_flash( MSG_NOT_FND_IN_DTBSE_DBG );
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature0);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature1);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature2);
#else
  LCD1602_write_text_from_flash_slow( MSG_NOT_FND_IN_DTBSE, text_delay );
  _delay_ms( user_read_info_delay );  // Take user some time to read this MSG
  LCD1602_Clear_display();
  LCD1602_write_text_from_flash_slow( MSG_SIGNATURE, text_delay );
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature0);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature1);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature2);
#endif
    _delay_ms( user_read_info_delay );  // Take user some time to read this MSG
    return i_status;
  }

  // Else chip is found, so we can write default fuses and write chip name
#if version == debug
  LCD1602_write_text( chip_attr.name );
#else
  LCD1602_write_text_slow( chip_attr.name , text_delay );
#endif

  i_status = HV_serial_set_fuses(chip_attr.hfuse, chip_attr.lfuse, osc_type);
  if(i_status != E_OK)
  {
    // LCD write - fuse write failed
    LCD1602_Clear_display();
    LCD1602_write_text_from_flash_slow(MSG_FUSE_WRITE_FAIL, text_delay);
    _delay_ms( user_read_info_delay );
    return i_status;
  }
  // If write was successful, then check if fuse bits were written correctly

  // High and low fuse Bytes
  uint8_t i_high = 0;
  uint8_t i_low = 0;

  i_status = HV_serial_read_fuses(&i_high, &i_low, osc_type);
  if(i_status != E_OK)
  {
    // LCD write - fuse read failed
    LCD1602_Clear_display();
    LCD1602_write_text_from_flash_slow(MSG_FUSE_READ_FAIL, text_delay);
    _delay_ms( user_read_info_delay );
    return i_status;
  }

  // Check if fuses are set correctly
  if((i_high != chip_attr.hfuse) || (i_low != chip_attr.lfuse))
  {
    // Fuses are not same. Must erase chip, so lock bits will be erased
    i_status = HV_serial_erase_chip(osc_type);
    if(i_status != E_OK)
    {
      // Write message to LCD
      LCD1602_Clear_display();
      LCD1602_write_text_from_flash_slow(MSG_CHIPERASE_FAIL, text_delay);
      _delay_ms( user_read_info_delay );
      return i_status;
    }

    i_status = HV_serial_set_fuses(chip_attr.hfuse, chip_attr.lfuse, osc_type);
    if(i_status != E_OK)
    {
      // LCD write - fuse write failed
      LCD1602_Clear_display();
      LCD1602_write_text_from_flash_slow(MSG_FUSE_WRITE_FAIL, text_delay);
      _delay_ms( user_read_info_delay );
      return i_status;
    }
    // If write was successful, then check if fuse bits were written correctly

    i_status = HV_serial_read_fuses(&i_high, &i_low, osc_type);
    if(i_status != E_OK)
    {
      // LCD write - fuse read failed
      LCD1602_Clear_display();
      LCD1602_write_text_from_flash_slow(MSG_FUSE_READ_FAIL, text_delay);
      _delay_ms( user_read_info_delay );
      return i_status;
    }
    // Again check if equal. If not  there is some undefined problem
    if((i_high != chip_attr.hfuse) || (i_low != chip_attr.lfuse))
    {
      return E_unknown;
    }
  }
  // Else everything seems to be normal

  LCD1602_set_address( 0x40 );
  LCD1602_write_text_from_flash_slow( MSG_UNLOCKED, text_delay );

  return i_status;
}




//=============================================================================

/**
 * \brief Try to load signature bits over parallel high voltage programming.
 *
 * @param p_signature_bytes Pointer to signature bytes
 * @param osc_type Oscillator type
 *
 * @return status code, where E_OK means "everything is OK", else there is\n
 * some problem (for example: "bad" oscillator).
 */
uint8_t HV_serial_load_signature_bits( signature_bytes_t *p_signature_bytes,
                                         uint8_t           osc_type)
{
  /* Dummy variable. When TX bits, then also read bits and these bits must
   * be stored somewhere. So there is dummy variable, which is used as
   * "black hole" (never read).
   */
  uint8_t i_dummy;

  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x08, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(0x00, 0x0C, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x68, &i_dummy, osc_type);
  //=================================| INSTR 4 |===============================
  HV_serial_send_receive_Byte(0x00, 0x6C, &(p_signature_bytes->i_signature0),
                              osc_type);

  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x08, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(0x01, 0x0C, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x68, &i_dummy, osc_type);
  //=================================| INSTR 4 |===============================
  HV_serial_send_receive_Byte(0x00, 0x6C, &(p_signature_bytes->i_signature1),
                              osc_type);

  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x08, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(0x02, 0x0C, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x68, &i_dummy, osc_type);
  //=================================| INSTR 4 |===============================
  HV_serial_send_receive_Byte(0x00, 0x6C, &(p_signature_bytes->i_signature2),
                              osc_type);

  /* Check if first byte is not wrong. If yes then signature bytes are probably
   * correct
   */
  if(p_signature_bytes->i_signature0 == 0x00)
  {
    return E_no_response;
  }
  if(p_signature_bytes->i_signature0 == 0xFF)
  {
    return E_incorrect_response;
  }

  // Else is all right
  return E_OK;
}

//=============================================================================

/**
 * \brief Read fuses
 *
 * @param p_i_high Pointer to variable. To this address will be written high
 *  fuse Byte.
 * @param p_i_low Pointer to variable. To this address will be written low fuse
 *  Byte.
 * @param osc_type Oscillator type
 * @return status code (E_OK means OK)
 */
inline uint8_t HV_serial_read_fuses(uint8_t *p_i_high, uint8_t *p_i_low,
                                    uint8_t osc_type)
{
  // Dummy variable
  uint8_t i_dummy;

  // Read fuses
  // LOW
  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x04, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(0x00, 0x68, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x6C, p_i_low, osc_type);


  // HIGH
  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x04, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(0x00, 0x7A, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x7E, p_i_high, osc_type);

  return E_OK;
}
//=============================================================================

/**
 * \brief Just try to set fuses
 *
 * @param i_high Fuse high Byte
 * @param i_low Fuse low Byte
 * @param osc_type Oscillator type
 * @return status code (E_OK means OK)
 */
inline uint8_t HV_serial_set_fuses(uint8_t i_high, uint8_t i_low,
                                   uint8_t osc_type)
{
  // Dummy variable
  uint8_t i_dummy;

  // 32 bit counter
  uint32_t i_cnt = 0;


  // Write fuses
  // LOW
  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x40, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(i_low, 0x2C, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x64, &i_dummy, osc_type);
  //=================================| INSTR 4 |===============================
  HV_serial_send_receive_Byte(0x00, 0x6C, &i_dummy, osc_type);
  // Wait until SDO goes high
  while(read_in_pin(&SDO_PORT, SDO_PIN) == 0)
  {
    i_cnt++;
    if(i_cnt >= FUSE_WRITE_DELAY)
    {
      return E_BSY_time_out;
    }
  }

  // HIGH
  i_cnt = 0;
  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x40, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(i_high, 0x2C, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x74, &i_dummy, osc_type);
  //=================================| INSTR 4 |===============================
  HV_serial_send_receive_Byte(0x00, 0x7C, &i_dummy, osc_type);
  // Wait until SDO goes high
  while(read_in_pin(&SDO_PORT, SDO_PIN) == 0)
  {
    i_cnt++;
    if(i_cnt >= FUSE_WRITE_DELAY)
    {
      return E_BSY_time_out;
    }
  }


  return E_OK;
}

//=============================================================================
/**
 * \brief Send command which erase whole chip
 *
 * Erase cause deleting lock bits
 * @param osc_type Oscillator type
 * @return status code (E_OK means all OK)
 */
inline uint8_t HV_serial_erase_chip(uint8_t osc_type)
{
  // Dummy variable
  uint8_t i_dummy;

  // 32 bit counter
  uint32_t i_cnt = 0;


  // Write fuses
  // LOW
  //=================================| INSTR 1 |===============================
  HV_serial_send_receive_Byte(0x80, 0x4C, &i_dummy, osc_type);
  //=================================| INSTR 2 |===============================
  HV_serial_send_receive_Byte(0x00, 0x64, &i_dummy, osc_type);
  //=================================| INSTR 3 |===============================
  HV_serial_send_receive_Byte(0x00, 0x6C, &i_dummy, osc_type);

  // Wait until SDO goes high
  while(read_in_pin(&SDO_PORT, SDO_PIN) == 0)
  {
    i_cnt++;
    if(i_cnt >= FUSE_WRITE_DELAY)
    {
      return E_BSY_time_out;
    }
  }

  return E_OK;
}
//=============================================================================
/**
 * \brief Simple function that only send and receive 1 Byte
 *
 * @param i_tx_SDI_Byte Data in Byte
 * @param i_tx_SII_Byte Instruction in Byte
 * @param p_rx_SDO_Byte Pointer to data out Byte
 * @param osc_type Oscillator type
 * @return status code (E_OK means all OK)
 */
inline uint8_t HV_serial_send_receive_Byte(uint8_t i_tx_SDI_Byte,
                                    uint8_t i_tx_SII_Byte,
                                    uint8_t *p_rx_SDO_Byte,
                                    uint8_t osc_type)
{
  // Simple counter
  uint8_t i;

  /* Format is simple. 0 (TX BYTE) 0 0 -> so transmitted is 11 bits
   * Receive format is following: (RX BYTE) 0 0 0. Again is transmitted 11 bits
   * This is bit by bit operations, so it is a little bit annoying.
   */
  // Clean SDO Byte
  *p_rx_SDO_Byte = 0x00;

  //==================================| Bit 0 |================================
  // Fist bit. SDI & SII always 0
  SDI_0;
  SII_0;
  give_CLK_pulse(osc_type);
  // Read MSB
  *p_rx_SDO_Byte |= read_in_pin(&SDO_PORT, SDO_PIN)<<7;
  //==============================| Bit 1 ~ bit 7 |============================
  /* This can be done in for cycle. It is more easy to write :)
   * Because we already read one bit, for will be done only 7 times.
   */
  for(i=7 ; i>0 ; i--)
  {
    // Set SDI
    if((i_tx_SDI_Byte & (1<<i)) == 0)
      SDI_0;
    else
      SDI_1;

    // Set SII
    if((i_tx_SII_Byte & (1<<i)) == 0)
      SII_0;
    else
      SII_1;
    // Send data
    give_CLK_pulse(osc_type);
    // Read data on bus
    *p_rx_SDO_Byte |= read_in_pin(&SDO_PORT, SDO_PIN)<<(i-1);
  }
  //==================================| Bit 8 |================================
  // Just send SDI and SII
  if((i_tx_SDI_Byte & (1<<0)) == 0)
    SDI_0;
  else
    SDI_1;

  if((i_tx_SII_Byte & (1<<0)) == 0)
    SII_0;
  else
    SII_1;
  // Send bit
  give_CLK_pulse(osc_type);
  //==================================| Bit 9 |================================
  // SDI & SII always 0
  SDI_0;
  SII_0;
  give_CLK_pulse(osc_type);
  //==================================| Bit 10 |===============================
  // SDI & SII always 0, but already set, so do not waste lines :)
  give_CLK_pulse(osc_type);

  return E_OK;
}
