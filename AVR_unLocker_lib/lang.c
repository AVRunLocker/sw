/* This file contains all messages used in AVR_unBlocker project. If needed,
 * then can be modify for another language without needs of rewrite program
 * core.
 * Note: Because all of this texts are static, it is useful save this strings
 * to flash memory. PSTR (from pgmspace.h) seems not doing everything as
 * expected, so it is done by hard way
 */

#include "lang.h"

/* Use macros written in lang.h. It is easy to edit strings */
/* General messages */
const char MSG_startup_info[] PROGMEM = MSG_startup_info_s;
const char MSG_OK[] PROGMEM = MSG_OK_s;

/* UnLocker messages */
const char MSG_NOT_FND_IN_DTBSE[] PROGMEM = MSG_NOT_FND_IN_DTBSE_s;
const char MSG_NOT_FND_IN_DTBSE_DBG[] PROGMEM = MSG_NOT_FND_IN_DTBSE_DBG_s;
const char MSG_SIGNATURE[] PROGMEM = MSG_SIGNATURE_s;
const char MSG_FUSE_WRITE_FAIL[] PROGMEM = MSG_FUSE_WRITE_FAIL_s;
const char MSG_FUSE_READ_FAIL[] PROGMEM = MSG_FUSE_READ_FAIL_s;
const char MSG_CHIPERASE_FAIL[] PROGMEM = MSG_CHIPERASE_FAIL_s;
const char MSG_UNLOCKED[] PROGMEM = MSG_UNLOCKED_s;

/* DC/DC converter */
const char MSG_CHECKING_DCDC_VCC[] PROGMEM = MSG_CHECKING_DCDC_VCC_s;
const char MSG_CHECKING_DCDC_12V[] PROGMEM = MSG_CHECKING_DCDC_12V_s;

const char MSG_VCC_3V3[] PROGMEM = MSG_VCC_3V3_s;
const char MSG_VCC_5V[] PROGMEM = MSG_VCC_5V_s;
const char MSG_VCC_UNKNOWN[] PROGMEM = MSG_VCC_UNKNOWN_s;

const char MSG_VCC_OVRVOLT_NM[] PROGMEM = MSG_VCC_OVRVOLT_NM_s;
const char MSG_VCC_UNDERVOLT_NM[] PROGMEM = MSG_VCC_UNDERVOLT_NM_s;

const char MSG_12V_OVRVOLT_SM[] PROGMEM = MSG_12V_OVRVOLT_SM_s;
const char MSG_12V_OVRVOLT_NM[] PROGMEM = MSG_12V_OVRVOLT_NM_s;
const char MSG_12V_UNDERVOLT_SM[] PROGMEM = MSG_12V_UNDERVOLT_SM_s;
const char MSG_12V_UNDERVOLT_NM[] PROGMEM = MSG_12V_UNDERVOLT_NM_s;

const char MSG_W8_LV[] PROGMEM = MSG_W8_LV_s;


/* SPI messages */
const char MSG_TRY_SPI8_CLASS[] PROGMEM = MSG_TRY_SPI8_CLASS_s;
const char MSG_TRY_SPI28_CLASS[] PROGMEM = MSG_TRY_SPI28_CLASS_s;
const char MSG_TRY_SPI40_CLASS[] PROGMEM = MSG_TRY_SPI40_CLASS_s;

/* High voltage programming messages */
const char MSG_TRY_HV_PARALLEL[] PROGMEM = MSG_TRY_HV_PARALLEL_s;
const char MSG_TRY_HV_SERIAL[] PROGMEM = MSG_TRY_HV_SERIAL_s;
