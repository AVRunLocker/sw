/**
 * \file
 *
 * \brief Routines for unlocking in SPI programming mode
 *
 * Created  19.06.2014\n
 * Modified 21.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#ifndef _SPI_MODE_H_
#define _SPI_MODE_H_
//================================| Includes |=================================
// Define integer types
#include <inttypes.h>

// Define common structures (such as signature_bytes_t)
#include "AVR_unLocker.h"

//===============================| Structures |================================
/**
 * \brief Structure for mapping SPI pins
 *
 * This is used for software SPI programming.
 * Used, when same SPI algorithm is done on different pins, so code have not be
 * re-written for every pin combination.
 */
typedef struct {
  volatile uint8_t * p_MISO_port_addr;  // Port address. Example: &PORTB
  uint8_t i_MISO_pin;                   // Pin number.   Example: 7 or PB7

  volatile uint8_t * p_MOSI_port_addr;
  uint8_t i_MOSI_pin;

  volatile uint8_t * p_SCK_port_addr;
  uint8_t i_SCK_pin;

} spi_pins_t ;


//===============================| Definitions |===============================

/* Name for easy SPI programming. Just link to existing pins, but name them
 * differently.
 */

/* SPI 8 pin class - for example ATtiny13 - socket DIP8 */
#define SPI8_MOSI_PORT  D0_PORT
#define SPI8_MOSI_PIN   D0_PIN
#define SPI8_MOSI_0     D0_0
#define SPI8_MOSI_1     D0_1

#define SPI8_MISO_PORT  D1_PORT
#define SPI8_MISO_PIN   D1_PIN
#define SPI8_MISO_0     D1_0
#define SPI8_MISO_1     D1_1

#define SPI8_SCK_PORT   D2_PORT
#define SPI8_SCK_PIN    D2_PIN
#define SPI8_SCK_0      D2_0
#define SPI8_SCK_1      D2_1

/* SPI 28 pin class - for example ATmega8 - socket DIP 28 */
#define SPI28_MOSI_PORT D3_PORT
#define SPI28_MOSI_PIN  D3_PIN
#define SPI28_MOSI_0    D3_0
#define SPI28_MOSI_1    D3_1

#define SPI28_MISO_PORT D4_PORT
#define SPI28_MISO_PIN  D4_PIN
#define SPI28_MISO_0    D4_0
#define SPI28_MISO_1    D4_1

#define SPI28_SCK_PORT  D5_PORT
#define SPI28_SCK_PIN   D5_PIN
#define SPI28_SCK_0     D5_0
#define SPI28_SCK_1     D5_1

/* SPI 40 pin class - for example ATmega32 - socket DIP 40 */
#define SPI40_MOSI_PORT D5_PORT
#define SPI40_MOSI_PIN  D5_PIN
#define SPI40_MOSI_0    D5_0
#define SPI40_MOSI_1    D5_1

#define SPI40_MISO_PORT D6_PORT
#define SPI40_MISO_PIN  D6_PIN
#define SPI40_MISO_0    D6_0
#define SPI40_MISO_1    D6_1

#define SPI40_SCK_PORT  D7_PORT
#define SPI40_SCK_PIN   D7_PIN
#define SPI40_SCK_0     D7_0
#define SPI40_SCK_1     D7_1

//===========================| Function prototypes |===========================
/**
 * \brief Try find target chip in DIP8
 *
 * @return If return zero -> all OK, else there was any problem.
 */
uint8_t Try_SPI8( void );

/**
 * \brief Analogy with function "Try_SPI8", but for 28 pin AVR
 *
 * @return status code
 */
uint8_t Try_SPI28( void );

/**
 * \brief Analogy with function "Try_SPI8", but for 40 pin AVR
 *
 * @return status code
 */
uint8_t Try_SPI40( void );


/**
 * \brief Most algorithms are common for every SW SPI communication
 *
 * So due to program size and make program more clear was created this function
 * @return status code
 */
uint8_t Try_SPI_general( spi_pins_t spi_pins );


/**
 * \brief Try to communicate with target chip.
 *
 * Assume, that oscillator is already set.
 * This algorithm is same for all oscillators. Use pointer to save signature
 * data directly, but also return status
 * @return status code
 */
uint8_t SPI_load_signature_bits( signature_bytes_t      *p_signature_bytes,
                                  uint8_t               osc_type,
                                  spi_pins_t            spi_pins);

/**
 * \brief Just send and receive 8 bits over software SPI.
 *
 * Need send map of software SPI pins as second argument. Also is needed give
 * information about used oscillator
 *
 * @return Received data
 */
uint8_t SPI_send_byte( uint8_t data , spi_pins_t spi_pins , uint8_t osc_type);


/**
 * \brief Wait for target chip until is ready.
 *
 * It needs only pin map and oscillator type.
 */
void SPI_wait_until_ready( spi_pins_t spi_pins, uint8_t osc_type );

#endif
