/**
 * \brief This is chip database for AVR unLocker
 *
 * ...but it can be use for any other projects too :)\n
 * Database contains: chip signature, name, hfuse, lfuse, efuse
 *
 * Created:  2012.10.25\n
 * Modified: 2016.08.09
 *
 * \author Martin Stejskal - martin.stej@gmail.com
 */

#ifndef _chip_database_lib
#define _chip_database_lib

#define E_chip_found_in_database          0
#define E_chip_not_found_in_database      254

/* Define how long can be chip name. It is recommended, that this number should
 * not exceeds 16, because of LCD options (1602 can show only 16 characters per
 * line) and also memory requirement rise.
 */
#define chip_name_max_length            16
//================================| Includes |=================================

#include <inttypes.h>           // Define integer types
#include <avr/pgmspace.h>       // Library for memory operations

//===============================| Structures |================================

/* To this structure will be saved chip attributes. It is easy to read to keep
 * these attributes together
 */
typedef struct{
  uint8_t hfuse;                        // High fuse
  uint8_t lfuse;                        // Low fuse
  uint8_t efuse;                        // Extended fuse (in some devices)
  char name[chip_name_max_length];      // Chip name in ASCII

} chip_attributes_t;


//===========================| Function prototypes |===========================

/**
 * \brief Try to find chip in database through signature.
 *
 * @param i_signature0 Signature byte 0 (LSB)
 * @param i_signature1 Signature byte 1
 * @param i_signature2 Signature byte 2 (MSB)
 * @param p_chip_attr Pointer to structure where data will be saved. It is\n
 * a little bit complicated to return whole structure in C, so let's make it\n
 * the easy way.
 *
 * @return Status which inform if chip was found or not
 */
uint8_t find_chip_in_database(
    char i_signature0, char i_signature1, char i_signature2,
    chip_attributes_t *p_chip_attr );

#endif


#ifndef NULL
  #define NULL (0)
#endif

#ifndef true
  #define true 1
#endif
#ifndef false
  #define false 0
#endif
