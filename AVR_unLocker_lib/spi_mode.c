/**
 * \file
 *
 * \brief Routines for unlocking in SPI programming mode
 *
 * Created  19.06.2014\n
 * Modified 21.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#include "spi_mode.h"

//================================| Functions |================================

/**
 * \brief Try find target chip in DIP8.
 *
 * Also load to structure "prog_result" loaded signature (if any).
 * Advantage of using pointer to this structure is, that any change is done
 * immediately.
 *
 * @return If return zero -> all OK, else there was any problem.
 */
uint8_t Try_SPI8( void )
{
  // Inform user
  LCD1602_Clear_display();
#if version == debug
  LCD1602_write_text_from_flash( MSG_TRY_SPI8_CLASS );
#else
  LCD1602_write_text_from_flash_slow( MSG_TRY_SPI8_CLASS, text_delay );
#endif


  // Map SPI pins
  spi_pins_t spi_pins;
  spi_pins.i_MISO_pin =         SPI8_MISO_PIN;
  spi_pins.p_MISO_port_addr =   &SPI8_MISO_PORT;
  spi_pins.i_MOSI_pin =         SPI8_MOSI_PIN;
  spi_pins.p_MOSI_port_addr =   &SPI8_MOSI_PORT;
  spi_pins.i_SCK_pin =          SPI8_SCK_PIN;
  spi_pins.p_SCK_port_addr =    &SPI8_SCK_PORT;

  /* Try SPI communication (all oscillators) and return status witch gives
   * function "Try_SPI_general"
   */
  return Try_SPI_general( spi_pins );
}

/*-----------------------------------------*/

/* Analogy with function "Try_SPI8", but for 28 pin AVR
 */
uint8_t Try_SPI28( void )
{
  // Inform user
  LCD1602_Clear_display();
#if version == debug
  LCD1602_write_text_from_flash( MSG_TRY_SPI28_CLASS );
#else
  LCD1602_write_text_from_flash_slow( MSG_TRY_SPI28_CLASS, text_delay );
#endif


  // Map SPI pins
  spi_pins_t spi_pins;
  spi_pins.i_MISO_pin =         SPI28_MISO_PIN;
  spi_pins.p_MISO_port_addr =   &SPI28_MISO_PORT;
  spi_pins.i_MOSI_pin =         SPI28_MOSI_PIN;
  spi_pins.p_MOSI_port_addr =   &SPI28_MOSI_PORT;
  spi_pins.i_SCK_pin =          SPI28_SCK_PIN;
  spi_pins.p_SCK_port_addr =    &SPI28_SCK_PORT;

  /* Try SPI communication (all oscillators) and return status witch gives
   * function "Try_SPI_general"
   */
  return Try_SPI_general( spi_pins );
}

/*-----------------------------------------*/

/* Analogy with function "Try_SPI8", but for 40 pin AVR
 */
uint8_t Try_SPI40( void )
{
  // Inform user
  LCD1602_Clear_display();
#if version == debug
  LCD1602_write_text_from_flash( MSG_TRY_SPI40_CLASS );
#else
  LCD1602_write_text_from_flash_slow( MSG_TRY_SPI40_CLASS, text_delay );
#endif


  // Map SPI pins
  spi_pins_t spi_pins;
  spi_pins.i_MISO_pin =         SPI40_MISO_PIN;
  spi_pins.p_MISO_port_addr =   &SPI40_MISO_PORT;
  spi_pins.i_MOSI_pin =         SPI40_MOSI_PIN;
  spi_pins.p_MOSI_port_addr =   &SPI40_MOSI_PORT;
  spi_pins.i_SCK_pin =          SPI40_SCK_PIN;
  spi_pins.p_SCK_port_addr =    &SPI40_SCK_PORT;

  /* Try SPI communication (all oscillators) and return status witch gives
   * function "Try_SPI_general"
   */
  return Try_SPI_general( spi_pins );
}

/*-----------------------------------------*/


/* Most algorithms are common for every SW SPI communication, so due to program
 * size and make program more clear was created this function
 */
uint8_t Try_SPI_general( spi_pins_t spi_pins )
{
  uint8_t i_status = E_unknown; /* Default state -> something is wrong (will be
                                 * changed)
                                 */


  // Set DC/DC out to Vcc - for safety
  DCDC_set_Vcc();

  /* RST_TARGET set to 0 -> transistor turn off -> RST on target will be high
   * -> can not reprogram now
   */
  RST_TARGET_0;


  /* To this variable will be saved loaded signature from target (if any).
   * This variable is cleaned every time in function "SPI_load_signature_bits",
   * so it do not have to be "cleaned"
   */
  signature_bytes_t i_signature_bytes;


  /* First let's assume, that internal oscillator is enabled. To variable
   * "osc_type" will be saved oscillator type (This is done automatically due
   * to reduce errors during developing)
   */
  uint8_t osc_type = Try_internal_oscillator();

  // Try communicate with target chip and load signature bits
  i_status = SPI_load_signature_bits( &i_signature_bytes,
                                      osc_type,
                                      spi_pins);

  // Test status -> if OK, then write fuses
  if ( i_status == E_OK )
  {
    goto SPI_general_Find_in_database;
  }
  // else try another oscillator
  osc_type = Try_external_XTAL();
  i_status = SPI_load_signature_bits( &i_signature_bytes,
                                      osc_type,
                                      spi_pins);
  // Test status -> if OK, then write fuses
  if ( i_status == E_OK )
  {
    goto SPI_general_Find_in_database;
  }
  // else try another oscillator
  osc_type = Try_external_RC();
  i_status = SPI_load_signature_bits( &i_signature_bytes,
                                      osc_type,
                                      spi_pins);
  // Test status -> if OK, then write fuses
  if ( i_status == E_OK )
  {
    goto SPI_general_Find_in_database;
  }
  // else try another oscillator
  osc_type = Try_external_CLK();
  i_status = SPI_load_signature_bits( &i_signature_bytes,
                                      osc_type,
                                      spi_pins);
  // Test status -> if OK, then write fuses
  if ( i_status == E_OK )
  {
    goto SPI_general_Find_in_database;
  }
  // else try another oscillator
  osc_type = Try_external_LF_RES();
  i_status = SPI_load_signature_bits( &i_signature_bytes,
                                      osc_type,
                                      spi_pins);
  if ( i_status == E_OK )
  {
    goto SPI_general_Find_in_database;
  }
  else  // else there is no any other oscillator
  {
    return i_status;   // Return error code
  }


SPI_general_Find_in_database:
  _delay_us(100);
  // Now find same signature bits in database

  // There will be saved default fuses and chip name
  chip_attributes_t chip_attr;

  // Fill "chip_attr" - try to find chip in database thru signature
  i_status = find_chip_in_database(
      i_signature_bytes.i_signature0,
      i_signature_bytes.i_signature1,
      i_signature_bytes.i_signature2,
      &chip_attr);

  LCD1602_Clear_display();

  // Test for status - if not found...
  if ( i_status == E_chip_not_found_in_database )
  {
#if version == debug
  LCD1602_write_text_from_flash( MSG_NOT_FND_IN_DTBSE_DBG );
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature0);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature1);
  LCD1602_write_hex_8bit(i_signature_bytes.i_signature2);
#else
    LCD1602_write_text_from_flash_slow( MSG_NOT_FND_IN_DTBSE, text_delay );
    _delay_ms( user_read_info_delay );  // Take user some time to read this MSG
    LCD1602_Clear_display();
    LCD1602_write_text_from_flash_slow( MSG_SIGNATURE, text_delay );
    LCD1602_write_hex_8bit(i_signature_bytes.i_signature0);
    LCD1602_write_hex_8bit(i_signature_bytes.i_signature1);
    LCD1602_write_hex_8bit(i_signature_bytes.i_signature2);
#endif
    _delay_ms( user_read_info_delay );  // Take user some time to read this MSG
    return i_status;
  }

  // Else chip is found, so we can write default fuses and Write chip name
#if version == debug
  LCD1602_write_text( chip_attr.name );
#else
  LCD1602_write_text_slow( chip_attr.name , text_delay );
#endif

  // First low fuses
  SPI_send_byte( 0xAC, spi_pins, osc_type );
  SPI_send_byte( 0xA0, spi_pins, osc_type );
  SPI_send_byte( 0x00, spi_pins, osc_type );     // Does not matter
  SPI_send_byte( chip_attr.lfuse, spi_pins, osc_type );

  // And wait until write is done
  SPI_wait_until_ready( spi_pins, osc_type );


  // High fuses
  SPI_send_byte( 0xAC, spi_pins, osc_type);
  SPI_send_byte( 0xA8, spi_pins, osc_type);
  SPI_send_byte( 0x00, spi_pins, osc_type);     // Does not matter
  SPI_send_byte( chip_attr.hfuse, spi_pins, osc_type);

  // And wait until write is done
  SPI_wait_until_ready( spi_pins, osc_type );

  // Extended fuses
  SPI_send_byte( 0xAC, spi_pins, osc_type);
  SPI_send_byte( 0xA4, spi_pins, osc_type);
  SPI_send_byte( 0x00, spi_pins, osc_type);     // Does not matter
  SPI_send_byte( chip_attr.efuse, spi_pins, osc_type);

  // And wait until write is done
  SPI_wait_until_ready( spi_pins, osc_type );

  LCD1602_set_address( 0x40 );
  LCD1602_write_text_from_flash_slow( MSG_UNLOCKED, text_delay );


  // If all OK
  i_status = E_OK;

  return i_status;
}


/*-----------------------------------------*/


/* Try to communicate with target chip. Assume, that oscillator is already set.
 * This algorithm is same for all oscillators. Use pointer to save signature
 * data directly, but also return status
 */
uint8_t SPI_load_signature_bits( signature_bytes_t      *p_signature_bytes,
                                  uint8_t               osc_type,
                                  spi_pins_t            spi_pins)
{
  /* RST_TARGET set to 0 -> transistor turn off -> RST on target will be high
   * -> can not reprogram now -> then set I/O
   */
  RST_TARGET_0;

  // For savings data coming from MISO
  uint8_t miso_data;

  // Reset signature[0..2] values
  p_signature_bytes->i_signature0 = 0x00;
  p_signature_bytes->i_signature1 = 0x00;
  p_signature_bytes->i_signature2 = 0x00;

  // Set default error code (do not know witch -> unknown)
  uint8_t i_status = E_unknown;



  // Set I/O - MISO, MOSI, SCK
  // MISO
  set_pin( spi_pins.p_MISO_port_addr,
           spi_pins.i_MISO_pin,
           dir_in  );
  // MOSI
  set_pin( spi_pins.p_MOSI_port_addr,
           spi_pins.i_MOSI_pin,
           dir_out );
  // SCK
  set_pin( spi_pins.p_SCK_port_addr,
           spi_pins.i_SCK_pin,
           dir_out );

  // Small delay
  _delay_ms( 1 );


  // now turn on RST transistor -> RST on target will be low
  RST_TARGET_1;
  // wait at least 20 ms
  for ( uint8_t i=0 ; i<21 ; i++)
  {
    give_CLK_pulse( osc_type );           // And if needed, send CLK pulses
  }

  // Now send programming enable code (1010 1100 ; 0101 0011 ; 8xX ; 8xX)
  SPI_send_byte( 0xAC, spi_pins, osc_type );            // Byte 1
  SPI_send_byte( 0x53, spi_pins, osc_type );            // Byte 2
  // Get echo back - now saved in "miso_data". MOSI data does not matter - 0x00
  miso_data = SPI_send_byte( 0x00, spi_pins, osc_type); // Byte 3
  //  MOSI data does not matter - 0x00
  SPI_send_byte( 0x00, spi_pins, osc_type );            // Byte 4

  // Test for response
  if ( miso_data == 0x00 )      // No response
  {
    i_status = E_no_response;
    return i_status;
  }
  if ( miso_data != 0x53 )      // Incorrect response
  {
    i_status = E_incorrect_response;
    return i_status;
  }

  /* Else everything looks ok, so...
   * read signature bits - code:  0011 0000 | 00xx xxxx | xxxx xxbb | oooo oooo
   * Read signature bit 0 - code: 0011 0000 | 0000 0000 | 0000 0000 | ???? ????
   */
  SPI_send_byte( 0x30, spi_pins, osc_type );
  SPI_send_byte( 0x00, spi_pins, osc_type );
  SPI_send_byte( 0x00, spi_pins, osc_type );
  // Now save value
  p_signature_bytes->i_signature0 = SPI_send_byte( 0x00, spi_pins, osc_type );

  // Read signature bit 1 - code: 0011 0000 | 0000 0000 | 0000 0001 | ???? ????
  SPI_send_byte( 0x30, spi_pins, osc_type );
  SPI_send_byte( 0x00, spi_pins, osc_type );
  SPI_send_byte( 0x01, spi_pins, osc_type );
  // Now save value
  p_signature_bytes->i_signature1 = SPI_send_byte( 0x00, spi_pins, osc_type );

  // And signature bit 2 - code: 0011 0000 | 0000 0000 | 0000 0010 | ???? ????
  SPI_send_byte( 0x30, spi_pins, osc_type );
  SPI_send_byte( 0x00, spi_pins, osc_type );
  SPI_send_byte( 0x02, spi_pins, osc_type );
  // Now save value
  p_signature_bytes->i_signature2 = SPI_send_byte( 0x00, spi_pins, osc_type );


  // Save to "i_status" value "E_OK" and make return
  return i_status = E_OK;
}


/*-----------------------------------------*/


/* Just send and receive 8 bits over software SPI. Received data are returned.
 * Need send map of software SPI pins as second argument. Also is needed give
 * information about used oscillator
 */
uint8_t SPI_send_byte( uint8_t data , spi_pins_t spi_pins , uint8_t osc_type )
{
  // Received data will be saved here
  uint8_t data_in = 0x00;       // Initialization value

  // MSB is sent first
  for ( uint8_t i=7 ; i<255 ; i-- )
    {
      // Test MOSI (data) if 1 or 0 - again use masking
      if ( (data & (1<<i)) != 0)
      {
        // Set MOSI to 1
        set_out_pin( spi_pins.p_MOSI_port_addr, spi_pins.i_MOSI_pin, 1);
      }
      // else no change is needed

      give_CLK_pulse( osc_type );

      // SCK to 1
      set_out_pin( spi_pins.p_SCK_port_addr, spi_pins.i_SCK_pin, 1);
      give_CLK_pulse( osc_type );

      // Now sample data on MISO and save to "data"
      data_in = data_in |
            (read_in_pin( spi_pins.p_MISO_port_addr, spi_pins.i_MISO_pin ))<<i;

      // SCK to 0
      set_out_pin( spi_pins.p_SCK_port_addr, spi_pins.i_SCK_pin, 0);
      // MOSI to 0
      set_out_pin( spi_pins.p_MOSI_port_addr, spi_pins.i_MOSI_pin, 0);
    }

  return data_in;
}

/*-----------------------------------------*/

/* Wait for target chip until is ready. It needs only pin map and oscillator
 * type.
 */
void SPI_wait_until_ready( spi_pins_t spi_pins, uint8_t osc_type )
{
  // Set busy flag to true
  uint8_t chip_busy = true;

  // And wait until chip is busy
  while( chip_busy == true )
  {
    // Give some extra pulses to target
    give_CLK_pulse( osc_type );
    // Read ready flag
    SPI_send_byte( 0xF0, spi_pins, osc_type );
    SPI_send_byte( 0x00, spi_pins, osc_type );
    SPI_send_byte( 0x00, spi_pins, osc_type );
    // When send last byte, then received byte is returned -> busy flag
    chip_busy = SPI_send_byte( 0x00, spi_pins, osc_type );
  }
}

