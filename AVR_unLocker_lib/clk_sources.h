/**
 * \file
 *
 * \brief Allow connect selected clock source to target chip
 *
 * Created  21.06.2014\n
 * Modified 21.06.2014
 *
 * \version 0.1
 * \author Martin Stejskal
 */

#ifndef _CLK_SOURCES_H_
#define _CLK_SOURCES_H_

//================================| Includes |=================================
// Defined uint8_t and other stuff
#include <inttypes.h>

// Define oscillator types, pins and delays
#include "settings.h"

// Delays
#include <util/delay.h>

// AVR I/O
#include <avr/io.h>

// Operations with I/O
#include "io_functions.h"




//===========================| Function prototypes |===========================
/* In dependency of used oscillator generate software CLK pulses to the target
 * chip if needed
 */
void give_CLK_pulse( uint8_t osc_type );


/* Disconnect any oscillators connected to the target chip (not needed when
 * internal oscillator running). Return oscillator type
 */
uint8_t Try_internal_oscillator(void);


/* Connect target chip to the external XTAL (4 MHz).  Return oscillator type
 */
uint8_t Try_external_XTAL(void);


/* Connect target chip to the RC oscillator.  Return oscillator type
 */
uint8_t Try_external_RC(void);


/* Set CLK pin as output and disable others oscillators. Pulses must be done by
 * software.  Return oscillator type
 */
uint8_t Try_external_CLK(void);

/* Set CLK pin as output and disable others oscillators. Pulses must be done by
 * software. In this mode give EXT CLK just one pulse. Return oscillator type
 */
uint8_t Try_external_CLK_1_pulse(void);



/* Disable all oscillators and enable only Low Frequency oscillator, that must
 * be software emulated. Return oscillator type
 */
uint8_t Try_external_LF_RES(void);

#endif
