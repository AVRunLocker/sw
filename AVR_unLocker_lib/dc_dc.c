/* This library affords set PWM that control DC/DC converter. It use ADC, PWM
 * channel and ADC interrupt. It allows user to set easy DC/DC voltage and if
 * some error occurs, then allow write brief info to LCD (1602).
 *
 *
 * Martin Stejskal - martin.stej@gmail.com
 */

#include "dc_dc.h"

/*-----------------------------------------*
 |     Global variables for this file      |
 *-----------------------------------------*/
// DC/DC modes - off/plus_5V_start/plus_5V/plus_12V_start/plus_12V
static volatile uint8_t i_DCDC_mode = DCDC_off;      // Default should be off

/* When voltage on DC/DC is stable and PWM is off, then to this variable is
 * saved information about power supply voltage (3.3V/5V/unknown). Default
 * should be unknown.
 */
static volatile uint8_t i_DCDC_Vcc_voltage = Vcc_is_unknown;

/* When start initialization, then is sampled (after some delay) value on DC/DC
 * output, when PWM is off -> DC/DC output should be approximately Vcc-0.2V ->
 * -> can determine, if power supply is 3.3 V or 5.0V. When sampled value is
 * smaller than 18 (witch is about 0.5 V), then probably diode on DC/DC is
 * broken, so it can make also easy diagnostic
 */
static volatile uint16_t i_DCDC_Vcc_value = 0x0000;


/* Counter for DC/DC start modes. If is reached "DCDC_MAX_start_count" and on
 * DC/DC output is still not stable ("DCDC_check_count" is not reached yet) then
 * DC/DC is evaluated as damaged -> show undervoltage message
 */
static uint16_t i_DCDC_num_of_step_CNT;

/* Counter for "i_DCDC_check_count" witch counts up if voltage on DC/DC output
 * is equal (on ADC) to required value (example: for 7.3V it is 265)
 */
static uint8_t i_DCDC_check_count;

/* Variable witch inform if DC/DC initialization is done or not. Default is
 * false
 */
static uint8_t i_DCDC_init_done = false;




/* Set DC/DC output voltage to +Vcc */
void DCDC_set_Vcc(void)
{
  i_DCDC_check_count = 0x00;            // Check count to zero
  i_DCDC_num_of_step_CNT = 0x0000;      // Number of steps to zero

  i_DCDC_mode = DCDC_safe_voltage;      // Change DC/DC mode to safe mode

  while ( i_DCDC_mode == DCDC_safe_voltage );   /* Wait until voltage on DC/DC
                                                 * fall under Vcc. When on
                                                 * DC/DC output is safe voltage,
                                                 * routine in interrupt change
                                                 * "i_DCDC_mode"
                                                 */

  i_DCDC_mode = DCDC_plus_Vcc;                  // Change mode
}

/* Set DC/DC output voltage to +12V */
void DCDC_set_12V(void)
{
  i_DCDC_check_count = 0x00;            // Check count to zero
  i_DCDC_num_of_step_CNT = 0x0000;      // Number of steps to zero

  i_DCDC_mode = DCDC_safe_voltage;      // Change DC/DC mode to safe mode


  while ( i_DCDC_mode == DCDC_safe_voltage );   /* Wait until voltage on DC/DC
                                                 * fall under Vcc. When on
                                                 * DC/DC output is safe voltage,
                                                 * routine in interrupt change
                                                 * "i_DCDC_mode"
                                                 */

  i_DCDC_mode = DCDC_plus_12V_start;     // Change mode
  while ( i_DCDC_mode == DCDC_plus_12V_start );  // And wait for mode change
}

/*-----------------------------------------*/

/* Set DC/DC mode to "DCDC_safe_voltage" and wait until voltage on DC/DC output
 * will be safe (Maximum Vcc)
 */
void DCDC_wait_for_safe_voltage(void)
{
  i_DCDC_mode = DCDC_safe_voltage;      // Change DC/DC mode to safe mode

  while ( i_DCDC_mode == DCDC_safe_voltage );   /* Wait until voltage on DC/DC
                                                 * fall under Vcc. When on
                                                 * DC/DC output is safe voltage,
                                                 * routine in interrupt change
                                                 * "i_DCDC_mode"
                                                 */
}

/*-----------------------------------------*/

/* ADC interrupt - when analog to digital conversation is done
 */
ISR( ADC_vect )
{
  uint16_t i_DCDC_feedback = ADC;       // Load value from ADC and save it

  // And set prescaler to 128 - if needed, then will be changed
  ADCSRA = ADCSRA | (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);


  // Now test for every single DC/DC mode
  /*-----------------------------------------*/
  // Test for "DCDC_safe_voltage" mode
  if ( (i_DCDC_mode == DCDC_safe_voltage) && (i_DCDC_init_done == true) )
    {
      // Turn off PWM
      OCR2 = 0x00;      /* And set OCR2 to 0x00 -> if not zero PWM will not
                         * turn off!
                         */

      TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                 * operation -> PWM output
                                                 * disabled
                                                 */

      // test if value on "i_DCDC_feedback" is smaller then Vcc
      if ( i_DCDC_feedback < (i_DCDC_Vcc_value+DCDC_Vcc_MAX_tolerance) )
        {
          i_DCDC_check_count++;         // Plus one success step


          if ( i_DCDC_check_count == DCDC_check_count )
            {   // If enough steps done
              i_DCDC_mode = DCDC_off;           // Change mode to DC/DC off

              i_DCDC_check_count = 0x00;        /* Restart counter for future
                                                 * use
                                                 */
            }
        }
      else
        {
          // If voltage on DC/DC is still high -> reset check counter
          i_DCDC_check_count = 0x00;
        }

      goto ADC_exit;                    // Nothing else to do
    }

  /*-----------------------------------------*/

  // Test, if PWM will be off, or on
  if ( (i_DCDC_mode == DCDC_off) && (i_DCDC_init_done == true) )
    {
      // Turn off PWM
      OCR2 = 0x00;      /* And set OCR2 to 0x00 -> if not zero PWM will not
                         * turn off!
                         */

      TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                 * operation -> PWM output
                                                 * disabled
                                                 */

      goto ADC_exit;    // Nothing else to do
    }


  /*-----------------------------------------*/
  // +12 V start mode - boost output voltage on DC/DC in short time
  if ( (i_DCDC_mode == DCDC_plus_12V_start) && (i_DCDC_init_done == true) )
    {
      // ON PWM -> set "1" to COM21
      TCCR2 = TCCR2 | (1<<COM21);

      // Again, test for overvoltage
      if (
       i_DCDC_feedback > (DCDC_PWM_Feedback_value_12V+DCDC_12V_MAX_tolerance)
         )
        {
          // Turn off PWM
          OCR2 = 0x00;      /* And set OCR2 to 0x00 -> if not zero PWM will not
                             * turn off!
                             */
          TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                     * operation -> PWM output
                                                     * disabled
                                                     */
#if DCDC_use_display == true    // Only if user want use LCD for errors
          LCD1602_Clear_display();                      // Clear LCD

          // Write message
          LCD1602_write_text_from_flash( MSG_12V_OVRVOLT_SM );
#endif
          while(1);                                     // Infinite loop
        }

      // If not overvoltage problem -> set prescaler to 2
      ADCSRA = ADCSRA & ~((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0));

      i_DCDC_num_of_step_CNT++;         // Plus one step
      // Test if limit is reached for number of steps -> undervoltage
      if ( i_DCDC_num_of_step_CNT > DCDC_MAX_start_count )
        {
          // Turn off PWM
          OCR2 = 0x00;      /* And set OCR2 to 0x00 -> if not zero PWM will not
                             * turn off!
                             */
          TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                     * operation -> PWM output
                                                     * disabled
                                                     */
#if DCDC_use_display == true    // Only if user want use LCD for errors
          LCD1602_Clear_display();                      // Clear LCD

          // Write message
          LCD1602_write_text_from_flash( MSG_12V_UNDERVOLT_SM );
#endif
          while(1);                                     // Infinite loop
        }

      // Test if "i_DCDC_check_count" is reached
      if ( i_DCDC_check_count == DCDC_check_count )
        {
          // If reached -> OK -> switch to normal mode
          i_DCDC_mode = DCDC_plus_12V;
          goto ADC_exit;
        }

      // Test if value on ADC is equal to defined value
      if ( i_DCDC_feedback == DCDC_PWM_Feedback_value_12V )
        {
          i_DCDC_check_count++;         // Plus one successful step
          goto ADC_exit;                // Nothing else to do
        }

      // Else is needed to increase or decrease PWM value
      goto change_PWM_value_12V;

    }


  /*-----------------------------------------*/
  // DC/DC +Vcc
  if ( (i_DCDC_mode == DCDC_plus_Vcc) && (i_DCDC_init_done == true) )
    {
      // Test for overvoltage
      if ( i_DCDC_feedback > (i_DCDC_Vcc_value+DCDC_Vcc_MAX_tolerance))
        {
          // Turn off PWM
          OCR2 = 0x00;      /* And set OCR2 to 0x00 -> if not zero PWM will not
                             * turn off!
                             */
          TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                     * operation -> PWM output
                                                     * disabled
                                                     */
#if DCDC_use_display == true    // Only if user want use LCD for errors
          LCD1602_Clear_display();                      // Clear LCD

          // Write message
          LCD1602_write_text_from_flash( MSG_VCC_OVRVOLT_NM );
#endif
          while(1);                                     // Infinite loop
        }

      // Test for undervoltage
      if ( i_DCDC_feedback < (i_DCDC_Vcc_value-DCDC_Vcc_MAX_tolerance))
        {
          // Turn off PWM
          TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                     * operation -> PWM output
                                                     * disabled
                                                     */
#if DCDC_use_display == true    // Only if user want use LCD for errors
          LCD1602_Clear_display();                      // Clear LCD

          // Write message
          LCD1602_write_text_from_flash( MSG_VCC_UNDERVOLT_NM );
#endif
          while(1);                                     // Infinite loop
        }

      goto ADC_exit;                // Nothing else to do
    }

  /*-----------------------------------------*/
  // +12 V on output - normal mode (start mode should be set before)
  if ( (i_DCDC_mode == DCDC_plus_12V) && (i_DCDC_init_done == true) )
    {
      // ON PWM -> set "1" to COM21
      TCCR2 = TCCR2 | (1<<COM21);

      // Test for overvoltage
      if (
       i_DCDC_feedback > (DCDC_PWM_Feedback_value_12V+DCDC_12V_MAX_tolerance)
         )
        {
          // Turn off PWM
          TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                     * operation -> PWM output
                                                     * disabled
                                                     */
#if DCDC_use_display == true    // Only if user want use LCD for errors
          LCD1602_Clear_display();                      // Clear LCD

          // Write message
          LCD1602_write_text_from_flash( MSG_12V_OVRVOLT_NM );
#endif
          while(1);                                     // Infinite loop
        }
      // Test for undervoltage
      if (
       i_DCDC_feedback < (DCDC_PWM_Feedback_value_12V-DCDC_12V_MAX_tolerance)
         )
        {
          // Turn off PWM
          TCCR2 = TCCR2 & ~((1<<COM21)|(1<<COM20)); /* PWM pin - normal port
                                                     * operation -> PWM output
                                                     * disabled
                                                     */
#if DCDC_use_display == true    // Only if user want use LCD for errors
          LCD1602_Clear_display();                      // Clear LCD

          // Write message
          LCD1602_write_text_from_flash( MSG_12V_UNDERVOLT_NM );
#endif
          while(1);                                     // Infinite loop
        }

      /* And set prescaler to 32. Because C is relative slower than assembler,
       * when clock is set to internal 1 MHz oscillator, program can not react
       * for changes in proper time -> faster sampling
       */
      ADCSRA = ADCSRA | (1<<ADPS2)|(1<<ADPS0);
      ADCSRA = ADCSRA & ~(1<<ADPS1);

change_PWM_value_12V:
      /* If feedback value is bigger then set -> decrease PWM value -> decrease
       * only if value in OCR2 is bigger than 0x00
       */
      if ( (i_DCDC_feedback > DCDC_PWM_Feedback_value_12V) && (OCR2 > 0) )
        {
          OCR2--;               // Decrease PWM value
          goto ADC_exit;        // All done -> exit
        }

      /* If feedback value is smaller then set -> increase PWM value -> increase
       * only if value in OCR2 is not bigger than DCDC_PWM_max
       */
      if (
       (i_DCDC_feedback < DCDC_PWM_Feedback_value_12V) && (OCR2 < DCDC_PWM_MAX)
         )
        {
          OCR2++;       // Increase PWM value
          goto ADC_exit;       // All done -> exit
        }
    }

  /*-----------------------------------------*/
  // Init mode -> needed do first, else DC/DC will not work
  if ( (i_DCDC_mode == DCDC_init) || (i_DCDC_init_done == false) )
    {
      /* If value on ADC is "same" as last -> increase counter
       * 1 LSB is about 2.5 mV -> 32 is about 80 mV -> should be bigger than
       * noise (value shifted by 5 to left -> div by 32).
       * In some cases was shift by 4 too low and process "freeze" at this
       * point because of noise. Now it works as expected
       */
      if ( (i_DCDC_feedback>>5) == (i_DCDC_Vcc_value>>5) )
        {
          i_DCDC_check_count++;
        }
      else
        {
          i_DCDC_check_count=0x00;      // Else restart
        }

      i_DCDC_Vcc_value = i_DCDC_feedback;       // And save sampled value

      // Test, if i_DCDC_check_count reached 0xFF -> stable output voltage
      if ( i_DCDC_check_count == 0xFF )
        {// If yes, then test for 5 V, or 3.3 V logic
          if (          // Test for 3V3 boundaries
           (i_DCDC_feedback >
            DCDC_PWM_Feedback_value_3V3-DCDC_Vcc_MAX_tolerance)

            &&
           (i_DCDC_feedback <
            DCDC_PWM_Feedback_value_3V3+DCDC_Vcc_MAX_tolerance)
             )
            {
              i_DCDC_Vcc_voltage = Vcc_is_3V3;  // Set variable (for future use)
              i_DCDC_mode = DCDC_off;           // And change mode
              i_DCDC_init_done = true;          // Initialization is done

#if (DCDC_use_display == true) && (version == debug)
              LCD1602_Clear_display();
              LCD1602_write_text_from_flash( MSG_VCC_3V3 );
              _delay_ms(500);
#endif
            }
          // test for 5V boundaries
          else if (
              (i_DCDC_feedback >
               DCDC_PWM_Feedback_value_5V-DCDC_Vcc_MAX_tolerance)

               &&
              (i_DCDC_feedback <
               DCDC_PWM_Feedback_value_5V+DCDC_Vcc_MAX_tolerance)
                )
            {
              // Set variable (for future use)
              i_DCDC_Vcc_voltage = Vcc_is_5V;

              i_DCDC_mode = DCDC_off;           // And change mode
              i_DCDC_init_done = true;          // Initialization is done

#if (DCDC_use_display == true) && (version == debug)
              LCD1602_Clear_display();
              LCD1602_write_text_from_flash( MSG_VCC_5V );
              _delay_ms(500);
#endif
               }
          else  // Else voltage on output is not known. Can work, but not sure
            {
              // Set variable (for future use)
              i_DCDC_Vcc_voltage = Vcc_is_unknown;
              i_DCDC_mode = DCDC_off;           // And change mode
              i_DCDC_init_done = true;          // Initialization is done

#if DCDC_use_display == true
              LCD1602_Clear_display();
              LCD1602_write_text_from_flash( MSG_VCC_UNKNOWN );
              _delay_ms(2000);
#endif
            }
        }


    }


  /*-----------------------------------------*/

ADC_exit:
  ADCSRA = ADCSRA | (1<<ADSC);  // And start next conversion

}
/*-----------------------------------------*/

/* Initialize PWM channel and ADC. User MUST ENABLE GLOBAL INTERRUPT! If user
 * set "DCDC_use_display" to true, then is needed initialize LCD before global
 * interrupt is set ( sei() )
 */
void init_DCDC_PWM_and_ADC(void)
{
  // PWM
  OCR2 = 0x00;                  // Set PWM pulse width to 0

  TCCR2 =
      (1<<WGM21)|(1<<WGM20)|    // Set fast PWM
      (0<<COM21)|(0<<COM20)|    /* Set normal pin operation -> PWM out is "off"
                                 * When we want on PWM, then COM21 must be set
                                 */
      (0<<CS22)|(0<<CS21)|(1<<CS10)|     // And set PWM clock

      (0<<FOC2)                 // Disable Force Output Compare
      ;

  // ADC
  ADMUX =
      (1<<REFS1)|(1<<REFS0)|    // Set internal reference as 2.56V
      (0<<ADLAR)|               // No, we won't left adjust result
      (DCDC_FB_pin)       /* And define, witch pin will be used as
                                 * feedback
                                 */
      ;
  ADCSRA =
      (1<<ADEN)|                // Turn on ADC
      (1<<ADSC)|                // Start AD conversion
      (0<<ADATE)|               // Off auto trigger
      (1<<ADIE)|                // Make interrupt active (when conversion done)
      (0<<ADPS2)|(0<<ADPS1)|(0<<ADPS0)  /* Now set prescaler to 2
                                         * Now need high-speed ADC. If
                                         * needed, then will be changed
                                         */
      ;

  /* And set DC/DC mode to init -> when global interrupt enabled -> in ADC
   * interrupt will be sampled value on off DC/DC -> should be approxmately
   * Vcc-0.2 V
   */
  i_DCDC_check_count = 0x00;            // And reset check counter
  i_DCDC_num_of_step_CNT = 0x0000;      // Also reset

  i_DCDC_init_done = false;             // Initialization is not complete yet

  i_DCDC_mode = DCDC_init;              // Set mode

  /* When global interrupt is enabled, then init function check voltage on DC/DC
   * out (PWM is off). When this init is done, then is user able change DC/DC
   * output values. Until is "i_DCDC_init_done" false, then "i_DCDC_mode" have
   * no effect when changed! So it is better set global interrupt and meanwhile
   * do some necessary routines. During this time ADC will sample DC/DC output
   * voltage and when output will be stable, then try determine output logic.
   * This take some time (depends on quality of power source, used capacitors
   * and so on) so do not waste time just waiting....
   */

  // And wait until DC/DC is ready
//  while( i_DCDC_mode == DCDC_init);

}


/**
 * \brief Just return number of actual mode
 * @return DCDC_off, DCDC_plus_Vcc, DCDC_plus_12V_start, DCDC_plus_12V,
 * DCDC_safe_voltage or DCDC_init
 */
inline uint8_t DCDC_mode(void)
{
  return i_DCDC_mode;
}
