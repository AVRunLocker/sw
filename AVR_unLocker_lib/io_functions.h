/* This library afford use every single pin on AVR. It is useful, when program
 * is almost done, but for hardware design is advantageous use different pin
 * (for example in case, when it simplify routing process). But as disadvantage
 * every function need some time to be done.
 *
 *
 * Martin Stejskal - martin.stej@gmail.com
 */


#ifndef _io_functions_library
#define _io_functions_library

/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/io.h>             // Inputs/outputs

/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/


/* Define direction for function set_pins()
 */
#define dir_out         1
#define dir_in          0

/*
define set_bit(reg,bit_num)     reg = (reg) | (1<<bit_num)
#define clr_bit(reg,bit_num)    reg = (reg) & (~(1<<bit_num))
 */

/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/

/* Set pin as output, or input. This depends on b_birection (0 - in ; 1 - out).
 * Input parameters are: port address (example: &PORTA), pin number (example:
 * 0~7, or PA3), direction (0 - in ; 1 - out). Advantage this function is that
 * user just define PORTx and pin number and this function set DDRx registers.
 * If is pin set as output, then will be set to zero. If will be set as input,
 * then pull-up resistor will be disabled.
 * Example: set_pin( &PORTD, 3, 1);     // Set PD3 as output
 * Notice: MUST be used '&' ,because we need to get address, not port "variable"
 */
void set_pin(
    volatile uint8_t * p_port_addr ,    // If no volatile -> warning
    uint8_t i_pin_number ,
    uint8_t b_direction );

/* Set output pin (must be set before) to logical value 0 or 1.
 * Example: set_out_pin( &PORTB, 3, 1); // Set PB3 to logic 1
 */

void set_out_pin(
    volatile uint8_t * p_port_addr ,    // If no volatile -> warning
    uint8_t i_pin_number ,
    uint8_t b_value );

/* Read value on defined pin (must be set as input before). Return value is
 * value, that is read.
 * Example: pin_value = read_in_pin( &PORTC, 5);        // Read data from PC5
 * Notice: MUST be used '&' ,because we need to get address, not port "variable"
 */
uint8_t read_in_pin(
    volatile uint8_t * p_port_addr,     // If no volatile -> warning
    uint8_t i_pin_number );

#endif
