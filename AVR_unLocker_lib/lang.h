/* This file contains all messages used in AVR_unBlocker project. If needed,
 * then can be modify for another language without needs of rewrite program
 * core.
 * Note: Because all of this texts are static, it is useful save this strings
 * to flash memory. PSTR (from pgmspace.h) seems not doing everything as
 * expected, so it is done by hard way
 */

#ifndef _lang_library
#define _lang_library

//================================| Includes |=================================
#include <avr/pgmspace.h>       /* For work with memory. There is used for save
                                 * text to flash, because these texts are not
                                 * changing.
                                 */
//===============================| Definitions |===============================

/* General messages */
#define MSG_startup_info_s      "AVR unLocker\n :: 0.3.2 ::"
#define MSG_OK_s                "OK"

/* UnLocker messages */
#define MSG_NOT_FND_IN_DTBSE_s  "Chip not found\n in database!"
#define MSG_NOT_FND_IN_DTBSE_DBG_s      "Not found.\nSignature "
#define MSG_SIGNATURE_s         "Signature: \n "
#define MSG_FUSE_WRITE_FAIL_s   "Writing fuses\n failed"
#define MSG_FUSE_READ_FAIL_s    "Reading fuses\n failed"
#define MSG_CHIPERASE_FAIL_s    "Chip erase\n failed"
#define MSG_UNLOCKED_s          " UnLocked ;-)"

/* DC/DC converter */
#define MSG_CHECKING_DCDC_VCC_s "Checking DC/DC\nVcc ... "
#define MSG_CHECKING_DCDC_12V_s "Checking DC/DC\n+12V ... "
#define MSG_VCC_3V3_s           "Power supply is\n  +3.3V"
#define MSG_VCC_5V_s            "Power supply is\n  +5V"
#define MSG_VCC_UNKNOWN_s       "Unknown power\n supply logic!"
#define MSG_VCC_OVRVOLT_NM_s    "Vcc overvoltage!\n (normal mode)"
#define MSG_VCC_UNDERVOLT_NM_s  "Vcc undervoltage!\n (normal mode)"
#define MSG_12V_OVRVOLT_SM_s    "+12V overvolt.!\n (start mode)"
#define MSG_12V_OVRVOLT_NM_s    "+12V overvolt.!\n (normal mode)"
#define MSG_12V_UNDERVOLT_SM_s  "+12V undervolt.!\n (start mode)"
#define MSG_12V_UNDERVOLT_NM_s  "+12V undervolt.!\n (normal mode)"
#define MSG_W8_LV_s             "W8 LV"

/* SPI messages */
#define MSG_TRY_SPI8_CLASS_s    "SPI programming\n DIP8 class"
#define MSG_TRY_SPI28_CLASS_s   "SPI programming\n DIP28 class"
#define MSG_TRY_SPI40_CLASS_s   "SPI programming\n DIP40 class"

/* High voltage programming messages */
#define MSG_TRY_HV_PARALLEL_s   "High voltage\nprog. (parallel)"
#define MSG_TRY_HV_SERIAL_s     "High voltage\n prog. (serial)"


/* Use macros written upper. It is easy to edit strings */
/* General messages */
extern const char MSG_startup_info[];
extern const char MSG_OK[];

/* UnLocker messages */
extern const char MSG_NOT_FND_IN_DTBSE[];
extern const char MSG_NOT_FND_IN_DTBSE_DBG[];
extern const char MSG_SIGNATURE[];
extern const char MSG_FUSE_WRITE_FAIL[];
extern const char MSG_FUSE_READ_FAIL[];
extern const char MSG_CHIPERASE_FAIL[];
extern const char MSG_UNLOCKED[];

/* DC/DC converter */
extern const char MSG_CHECKING_DCDC_VCC[];
extern const char MSG_CHECKING_DCDC_12V[];

extern const char MSG_VCC_3V3[];
extern const char MSG_VCC_5V[];
extern const char MSG_VCC_UNKNOWN[];

extern const char MSG_VCC_OVRVOLT_NM[];
extern const char MSG_VCC_UNDERVOLT_NM[];

extern const char MSG_12V_OVRVOLT_SM[];
extern const char MSG_12V_OVRVOLT_NM[];
extern const char MSG_12V_UNDERVOLT_SM[];
extern const char MSG_12V_UNDERVOLT_NM[];

extern const char MSG_W8_LV[];


/* SPI messages */
extern const char MSG_TRY_SPI8_CLASS[];
extern const char MSG_TRY_SPI28_CLASS[];
extern const char MSG_TRY_SPI40_CLASS[];

/* High voltage programming messages */
extern const char MSG_TRY_HV_PARALLEL[];
extern const char MSG_TRY_HV_SERIAL[];



#endif
