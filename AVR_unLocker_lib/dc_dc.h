/* This library affords set PWM that control DC/DC converter. It use ADC, PWM
 * channel and ADC interrupt. It allows user to set easy DC/DC voltage and if
 * some error occurs, then allow write brief info to LCD (1602).
 *
 *
 * Martin Stejskal - martin.stej@gmail.com
 */


#ifndef _dc_dc_library
#define _dc_dc_library

  /* Define if user want see errors on LCD or not. If set to true -> LCD init
   * must be done before global interrupt is enabled!
   */
#define DCDC_use_display true
/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
#include <avr/io.h>
#include <inttypes.h>           // Define integer types
#include <avr/interrupt.h>      // Interrupts

#include "settings.h"           // Just because F_CPU variable
#include "lang.h"               // For messages

  // If user want see errors on LCD
#if DCDC_use_display == true
#include "LCD_1602.h"         // If true -> include LCD library
#endif
/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/

/* DC/DC mode */
#define DCDC_off                0       /* DC/DC output voltage is approximately
                                         * Vcc-0.2V (but output is not
                                         * controlled. PWM is just off)
                                         */

#define DCDC_plus_Vcc           1       /* Off PWM -> output should be Vcc-0.2 V
                                         * but in this mode is DC/DC output is
                                         * checked if is not too low, or too
                                         * high (For example, if there is not
                                         * short on DC/DC out)
                                         */

#define DCDC_plus_12V_start     2       /* Boost DC/DC voltage up to 12 V. If
                                         * voltage level reached -> switch to
                                         * "DCDC_plus_12V" mode
                                         */

#define DCDC_plus_12V           3       /* Try sustain 12 V on DC/DC output.
                                         * If some problem occurs, then turn off
                                         * PWM and voltage on DC/DC fall down.
                                         */

#define DCDC_safe_voltage       4       /* Wait until voltage on DC/DC fall on
                                         * "safe" voltage -> usually Vcc. When
                                         * done, then set DC/DC mode to DCDC_off
                                         */

#define DCDC_init               5       /* Sample voltage on DC/DC when PWM is
                                         * off -> on DC/DC out should be
                                         * Vcc-0.2 V -> can determine if system
                                         * runs on +5 V, or 3.3 V power supply.
                                         */

/*-----------------------------------------*/

/* Vcc definitions - for easy orientation in code*/
#define Vcc_is_3V3              0x33
#define Vcc_is_5V               0x50
#define Vcc_is_unknown          0xFF


/* Following lines SHOULD NOT be changed, until You REALLY KNOW what are You
 * DOING! Changes may DAMAGE DEVICE!
 */

/* Define most pins */

/* Define witch pin is used for DC/DC feedback (only PORTA)
 */
#define DCDC_FB_pin PA1

/* Define max PWM pulse width. When 255 (0xFF), DC/DC not working working at
 * all and wide pulses can damage transistor, or inductor (When transistor
 * is switched on, then current is limited only Resr of inductor plus serial
 * resistor). DO NOT CHANGE THIS VARIABLE UNTIL YOU REALLY KNOW WHAT ARE YOU
 * DOING!
 *
 * For debug purposes should be minimum (about 16V generated ->30). Else can
 * be about 45, but beware! Higher number means faster start DC/DC converter
 * but more heat on switching transistor. If is used bipolar transistor, then
 * can be about 60. Lower value means safer, but slower start. High speed may
 * cause overvoltage messages!
 * Value bigger than 100 usually do not have good result. Usually it makes
 * output voltage even lower! For maximum efficiency use smallest number as
 * possible
 */
#define DCDC_PWM_MAX 30

/* Define "time", on what DC/DC should start (reach requested voltage), else
 * DC/DC will be values as damaged. For debug purposes can be 0x1E00, else
 * 0xFFFF. Higher means better stability - more accurate, more save)
 */
#if version == debug
#define DCDC_MAX_start_count 0x3000
#else
#define DCDC_MAX_start_count 0xFFFF
#endif

/* Define how many times must be selected voltage on output equal
 * selected value on  ADC to skip to the next mode (from start to normal
 * mode, etc). How many times must be DC/DC output value be equal with value
 * witch was set by user. Bigger means safer, but it takes more time.
 * Recommended is 255!
 */
#define DCDC_check_count 255


/* DC/DC feedback values - when this value is on ADC, then right voltage is on
 * DC/DC converter. Value is calculated following way:
 *  Feedback_value = ((Vdcdc/(Rf1+Rf2))*Rf1) / (Vref/1023)
 *  where: Vdcdc - required voltage on DC/DC output
 *         Rf1   - Value of resistor Rf1 (used in feedback - connected to GND)
 *         Rf2   - value of resistor Rf2 (connected to DC/DC out)
 *         Vref  - ADC reference voltage - in this case it is 2.56 V (internal
 *                 reference)
 */
#define DCDC_PWM_Feedback_value_12V 450 /* Because of protective resistor
                                         * can be a little bit higher.
                                         * Also when in serial high voltage
                                         * current consumption is higher, so
                                         * voltage drop on protective resistor
                                         * increase.
                                         * Experimentally was find out, that
                                         * for parallel high voltage
                                         * programming is enough 10 V.
                                         * For serial high voltage is needed
                                         * about 12V.
                                         * Because there definitely should be
                                         * some reserve, value is set to 12.5V
                                         */
// 435 = 12V save value 4 parallel programming
// 450 = 12.5V
// 472 = 13V

#define DCDC_PWM_Feedback_value_3V3 112 /* Calculated for 3.1 V (3.3 V - 0.2 V
                                         * -> 0.2 V is on schottky diode)
                                         */

#define DCDC_PWM_Feedback_value_5V  174 /* Calculated for 4.8 V (5-0.2)
                                         */


/* Gives DC/CD a little bit tolerance for DC/DC output (due to current
 * dependency)
 */
#if version == debug    // If in debug -> more strict
#define DCDC_Vcc_MAX_tolerance 9        // 9 = approx. +-0.25 V
#else
#define DCDC_Vcc_MAX_tolerance 18       // 18 = approx. +-0.5 V
#endif

/* Same as DCDC_5V_MAX_tolerance, but for DC/DC voltage 12 V. There can be
 * little overvoltage, because of pulse characteristic switched power supply
 */
#if version == debug    // If in debug -> more strict
#define DCDC_12V_MAX_tolerance 14       // 14 = approx. +-0.385 V
#else
#define DCDC_12V_MAX_tolerance 18       // 18 = approx. +-0.5 V
#endif

// Just for case
#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif


/*-----------------------------------------*
 |         Function prototypes             |
 *-----------------------------------------*/
/* Set DC/DC output voltage to +5V */
void DCDC_set_Vcc(void);

/* Set DC/DC output voltage to +12V */
void DCDC_set_12V(void);

/* Set DC/DC mode to "DCDC_safe_voltage" and wait until voltage on DC/DC output
 * will be safe (Maximum Vcc)
 */
void DCDC_wait_for_safe_voltage(void);

/* ADC interrupt - when analog to digital conversation is done. */
ISR( ADC_vect );

/* Initialize PWM channel and ADC. User MUST ENABLE GLOBAL INTERRUPT! If user
 * set "DCDC_use_display" to true, then is needed initialize LCD before global
 * interrupt is set ( sei() )
 */
void init_DCDC_PWM_and_ADC(void);

/**
 * \brief Just return number of actual mode
 * @return DCDC_off, DCDC_plus_Vcc, DCDC_plus_12V_start, DCDC_plus_12V,
 * DCDC_safe_voltage or DCDC_init
 */
uint8_t DCDC_mode(void);


#endif
