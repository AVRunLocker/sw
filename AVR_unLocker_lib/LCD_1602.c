/* This is library for communication with 1602 (16x2 characters) LCDs.
 * It is common used LCD. This library use 4 data connection, witch reduce
 * number of used pins to minimum.
 *
 *
 * Martin Stejskal - martin.stej@gmail.com
 */

#include "LCD_1602.h" // Defined pins, function prototypes


/* Prepare ports and initialize LCD, so next communication with LCD will be
 * easy. And clear display
 */
void LCD1602_init(void)
{
  // OK, first is needed to set I/O
  LCD1602_set_IO();

  _delay_ms(10);        /* Can not initialize LCD "right now", must wait at
                         * least 10ms
                         */

  // Set 4 bit data transmit
  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_0;
  LCD_D5_1;
  LCD_D4_0;     // Data length: 4 bits
  LCD1602_Signal_enable();      // And confirm data on bus


  // Set function set
  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_0;
  LCD_D5_1;
  LCD_D4_0;     // Data length: 4 bits
  LCD1602_Signal_enable();      // And confirm data on bus

  LCD_RS_0;
  LCD_D7_1;     // N - 1 means 2 lines ; 0 - 1 line
  LCD_D6_1;     // F - font size        ; (0 "not working")
  LCD_D5_0;
  LCD_D4_0;
  LCD1602_Signal_enable();      // Confirm data on bus


  // Display and cursor settings
  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_0;
  LCD_D5_0;
  LCD_D4_0;
  LCD1602_Signal_enable();

  LCD_RS_0;
  LCD_D7_1;
  LCD_D6_1;     // Display ON - 1

  // Cursor settings
#if LCD_defaut_cursor == cursor_off
  LCD_D5_0;     // Cursor OFF
#else
  LCD_D5_1;     // Cursor ON - 1
#endif

#if LCD_defaut_cursor == cursor_blink
  LCD_D4_1;     // Blinking cursor ON - 1
#else
  LCD_D4_0;     // Blinking cursor OFF - 0
#endif

  LCD1602_Signal_enable();


  // Entry mode set
  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_0;
  LCD_D5_0;
  LCD_D4_0;
  LCD1602_Signal_enable();

  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_1;
  LCD_D5_1;     // Increment cursor - 1
  LCD_D4_0;     // No shift - 0
  LCD1602_Signal_enable();


  // And clear LCD
  LCD1602_Clear_display();
}

/*-----------------------------------------*/

// Delete all symbols on LCD
void LCD1602_Clear_display(void)
{
  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_0;
  LCD_D5_0;
  LCD_D4_0;
  LCD1602_Signal_enable();

  LCD_RS_0;
  LCD_D7_0;
  LCD_D6_0;
  LCD_D5_0;
  LCD_D4_1;
  LCD1602_Signal_enable();
}


/*-----------------------------------------*/

// Write one symbol on LCD. Example: LCD1602_write_char('C'); // Write "C"
void LCD1602_write_char( uint8_t i_char )
{
  LCD_RS_1;     // LCD RS set to "1"

  LCD1602_send_8bits( i_char );     // And send raw data

}

/*-----------------------------------------*/

// Write whole string on LCD. Example LCD1602_write_text("test text");

void LCD1602_write_text( char * s_text )
{
  while( * s_text != NULL )
    {
      /* Test for "\n" character (new line). If found, set address on LCD to
       * 0x40 -> first symbol on second line on LCD
       */
      if ( * s_text == '\n' )
        {
          LCD1602_set_address( 0x40 );  // Set LCD cursor to second line
          // And ignore symbol \n -> move to next symbol
          s_text++;
        }
      LCD1602_write_char( * s_text);    // Write one symbol to LCD
      s_text++;                         // Move to next symbol/character
    }
}

/*-----------------------------------------*/

/* Write whole string on LCD, but insert time delay between single characters.
 * Example: LCD1602_write_text_slow( "Some text", 200);
 * Write string on LCD and between every symbol (character) insert time delay
 * 200 ms
 */
void LCD1602_write_text_slow( char * s_text , const int i_delay )
{
  while( * s_text != NULL )
    {
      /* Test for "\n" character (new line). If found, set address on LCD to
       * 0x40 -> first symbol on second line on LCD
       */
      if ( * s_text == '\n' )
        {
          LCD1602_set_address( 0x40 );  // Set LCD cursor to second line
          // And ignore symbol \n -> move to next symbol
          s_text++;
        }
      LCD1602_write_char( * s_text);    // Write one symbol to LCD
      s_text++;                         // Move to next symbol/character
      _delay_ms( i_delay );             // Insert time delay
    }
}

/*-----------------------------------------*/

/* Very similar to "LCD1602_write_text", but instead of reading string
 * from SRAM it reads data from flash memory, witch is useful on embedded
 * systems with small SRAM.
 * Example: LCD1602_write_text_from_flash( string_variable );
 * Where string variable is declared this way:
 *  const char string_variable[] PROGMEM = "Some text to\n be written";
 */
void LCD1602_write_text_from_flash( const char * s_text_flash )
{
  while ( pgm_read_byte(s_text_flash) != NULL )
  {
    if ( pgm_read_byte(s_text_flash) == '\n' )
    {
      LCD1602_set_address( 0x40 );
      s_text_flash++;
    }
    LCD1602_write_char( pgm_read_byte(s_text_flash) );
    s_text_flash++;
  }
}

/*-----------------------------------------*/

/* Again, similar to "LCD1602_write_text_slow", but instead of reading string
 * from SRAM it reads data from flash memory and give some time delay between
 * every single symbol. Delay is in ms.
 * Example: LCD1602_write_text_from_flash( string_variable, 100 );
 * Where string variable is declared this way:
 *  const char string_variable[] PROGMEM = "Some text to\n be written";
 * Between every symbol will be insert 100ms delay.
 */
void LCD1602_write_text_from_flash_slow(
    const char * s_text_flash , int i_delay )
{
  while ( pgm_read_byte(s_text_flash) != NULL )
  {
    if ( pgm_read_byte(s_text_flash) == '\n' )
    {
      LCD1602_set_address( 0x40 );
      s_text_flash++;
    }
    LCD1602_write_char( pgm_read_byte(s_text_flash) );
    s_text_flash++;
    _delay_ms( i_delay );
  }
}

/*-----------------------------------------*/

/* Write input 8 bit value as HEX number.
 * Example: LCD1602_write_hex_8bit( 170 );
 * Write AA to LCD (170 in dec is 0xAA)
 */
void LCD1602_write_hex_8bit( uint8_t i_data )
{
  // First is needed to split 8 bit value to 2x4 bits

  // First should be written MSB to LCD
  uint8_t byte_part = (i_data & 0b11110000)>>4; /* Copy 4 MSB bits and rotate
                                                 * them by 4 to the right
                                                 */
  LCD1602_write_one_digit( byte_part ); // Write 4 bits as HEX value (0~F)

  // Now LSB
  byte_part = (i_data & 0b00001111);    // No shift needed, just copy LSB
  LCD1602_write_one_digit( byte_part ); // And write to LCD
}

/*-----------------------------------------*/

/* Write input 8 bit value as HEX number and insert between single characters
 * delay.
 * Example: LCD1602_write_hex_8bit( 170 , 35);
 * Write AA to LCD (170 in dec is 0xAA) with 35 ms delay
 */
void LCD1602_write_hex_8bit_slow( uint8_t i_data , int i_delay)
{
  // First is needed to split 8 bit value to 2x4 bits

  // First should be written MSB to LCD
  uint8_t byte_part = (i_data & 0b11110000)>>4; /* Copy 4 MSB bits and rotate
                                                 * them by 4 to the right
                                                 */
  LCD1602_write_one_digit( byte_part ); // Write 4 bits as HEX value (0~F)
  _delay_ms( i_delay );

  // Now LSB
  byte_part = (i_data & 0b00001111);    // No shift needed, just copy LSB
  LCD1602_write_one_digit( byte_part ); // And write to LCD
  _delay_ms( i_delay );

}

/*-----------------------------------------*/

/* Set address on LCD. That means cursor position. "First" line has address
 * range from 0x00 to 0x27. The "second" line begins at 0x40 thru 0x67. If
 * invalid address given, then return non-zero value
 */
uint8_t LCD1602_set_address( uint8_t i_addr )
{
  // Test for validity input variable
  if ( (i_addr > 0x67 ) || ( (i_addr > 0x27) && (i_addr < 0x40) )  )
    {
      return 1; // Return non-zero value
    }

  /* Else variable seems to be OK -> MSB must be 1 -> add 1 to i_addr to MSB
   * position
   */
  i_addr = i_addr | (1<<7);

  LCD_RS_0;

  LCD1602_send_8bits( i_addr ); // And send data to bus

  return 0;
}

/*-----------------------------------------*/

/* Input value - 0~F. Write as one digit to LCD.
 * Example: LCD1602_write_one_digit( 0xC );
 * Write to LCD symbol C
 */
void LCD1602_write_one_digit( uint8_t digit )
{
  // Test for input value. If bigger than 0xF -> can not display -> show "?"
  if ( digit > 0xF )
    {
      LCD1602_write_char( '?');
      return;
    }

  // Test for range 0~9
  if ( digit < 10 )
    {
      // Give write_char ASCII number
      LCD1602_write_char( 48 + digit );
    }
  else  // A~F
    {
      // Again, just give ASCII number
      LCD1602_write_char( 55 + digit ); // 65 is A symbol -> A is 10 -> 65-10
    }
}

/*-----------------------------------------*/


/* Send 8 data bits to LCD. Do no set RS signal, so programmer must set it
 * before this function is called.
 */
void LCD1602_send_8bits( uint8_t i_data )
{
  // First we need send high 4 bits - set high bits
  if ( ((1 << 7) & i_data ) > 0 )       /* Little complicated. So, first is
                                         * created "mask" 0b10000000. Then is
                                         * done AND operation on bit level with
                                         * variable "i_data". So, if on MSB was
                                         * logical 1, then logical 1 is copy to
                                         * condition. Else is copy 0 of MSB.
                                         * Anyway, if value is non-zero, then
                                         * is needed to set pin to 1. Else set
                                         * to 0.
                                         */
    {   // Is MSB is 1 -> set 1
      LCD_D7_1;
    }
  else  // set 0
    {
      LCD_D7_0;
    }

  if ( ((1 << 6) & i_data ) > 0 )      // x << y - shift "x" by "y" bits to left
    {   // Is bit is is 1 -> set 1
      LCD_D6_1;
    }
  else  // set 0
    {
      LCD_D6_0;
    }

  if ( ((1 << 5) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_D5_1;
    }
  else  // set 0
    {
      LCD_D5_0;
    }

  if ( ((1 << 4) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_D4_1;
    }
  else  // set 0
    {
      LCD_D4_0;
    }

  LCD1602_Signal_enable();      // Send enable -> LCD recieve high 4 bits


  // Now send low 4 bits

  if ( ((1 << 3) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_D7_1;
    }
  else  // set 0
    {
      LCD_D7_0;
    }

  if ( ((1 << 2) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_D6_1;
    }
  else  // set 0
    {
      LCD_D6_0;
    }

  if ( ((1 << 1) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_D5_1;
    }
  else  // set 0
    {
      LCD_D5_0;
    }

  if ( ((1 << 0) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_D4_1;
    }
  else  // set 0
    {
      LCD_D4_0;
    }

  LCD1602_Signal_enable();      // Again, confirm data on bus
}

/*-----------------------------------------*/

// Low-level function. Just generate one enable pulse for LCD
void LCD1602_Signal_enable(void)
{
  //_delay_ms(1);
  LCD_E_1;       // Set E to 1
  _delay_ms(1);
  LCD_E_0;       // Set E to 0
  _delay_ms(1);
}

/*-----------------------------------------*/

// Prepare pins, witch will be used for communication with LCD
void LCD1602_set_IO(void)
{
  // First set Enable pin
  set_pin( &LCD_E_PORT, LCD_E_PIN, dir_out);

  // Set RS pin
  set_pin( &LCD_RS_PORT, LCD_RS_PIN, dir_out);

  // Set D4 to D7
  set_pin( &LCD_D4_PORT, LCD_D4_PIN, dir_out);
  set_pin( &LCD_D5_PORT, LCD_D5_PIN, dir_out);
  set_pin( &LCD_D6_PORT, LCD_D6_PIN, dir_out);
  set_pin( &LCD_D7_PORT, LCD_D7_PIN, dir_out);
}

/*-----------------------------------------*/
