#!/bin/bash
device="atmega32";
fn="AVR_unLocker"; # fn - filename
dir_lib="AVR_unLocker_lib"
dg="Debug"
sum=2;	# Number of steps
cnt=1;	# Actual step
programmer=dragon_isp	# Avrdude programmer name
programmer=usbasp	# Avrdude programmer name

# Create Debug directory - just for case
mkdir $dg 2>/dev/null;

make &&
 echo " HEX file generated      $cnt/$sum" && let cnt++ &&

avrdude -c $programmer -p m32 -U flash:w:$fn.hex && # upload HEX file
 echo " Upload successful       $cnt/$sum" && let cnt++ # && let cnt++;
#sleep 1s;
# Restart target chip one more time (because of LCD)
#avrdude -c $programmer -p m32 2>/dev/null;

# test for operations result
 if [ $sum -ge $cnt ];	# ge, gt, le, lt, ne, eq
 then
  echo "/-----------------\\";
  echo "| Process failed! |";
  echo "\\-----------------/";
 else
  echo "/------------\\";
  echo "| Process OK |";
  echo "\\------------/";
 fi
