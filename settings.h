/* This contains basic setting of AVR unLocker
 */

/* Speed of CPU. This is used for delay routines
 */
#ifndef _settings_library_
#define _settings_library_


/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/
// Define what configuration will be used. Options are: debug/release
#define version release

/* Define delays (in ms) between printed characters on LCD
 */
// Info message on program startup
#define startup_info_delay      85

// Ordinary delay for most text shows on LCD
#define text_delay              30

/* When some text is written on LCD, then take user some time to read message
 * (This is used for quick info messages, not so important)
 */
#define user_read_quick_info_delay         730

/* When some text is written on LCD, then take user some extra time
 */
#define user_read_info_delay               7000

/* How many times SPI programming try communicate with target before try
 * another oscillator
 */
#define SPI_programming_try 3

/* Define time in ms witch unBlocker wait for RDY from target. Else will be
 * selected next oscillator
 */
#define HV_programming_wait_for_RDY 20

/* How many pulses give to target when is set "external clock" oscillator.
 * In basic is enough 2 cycles, but if target has enabled prescaller (usually
 * set to 8), then is needed 8*2 pulses. So Minimum is 16, but just for case
 * add some extra pulses
 */
#define num_ext_CLK_pulses 18

/* Same as "num_ext_CLK_pulses", but for low frequency oscillator. This number
 * should be at least 18 for case.
 */
#define num_ext_LF_OSC_pulses 20

/* Pin, witch is connected to the reset control transistor */
#define RST_TARGET_PORT PORTA   // Define PORT
#define RST_TARGET_PIN  7       // Define pin number on PORT


/* Keyboard enable - enable keyboard only if needed */
#define KEYB_E_PORT     PORTA   // Define PORT
#define KEYB_E_PIN      6       // Define pin number on PORT


/* Connect external XTAL to target chip */
#define EXT_XTAL_PORT   PORTA   // Define PORT
#define EXT_XTAL_PIN    5       // Define pin number on PORT// Set EXT_XTAL to logical 0


/* Connect external RC to target chip */
#define EXT_RC_PORT     PORTA   // Define PORT
#define EXT_RC_PIN      4       // Define pin number on PORT


/* Turn off all external oscillators. If CLK to target needed, then must be
 * done by software.
 */
#define EXT_CLK_PORT    PORTA   // Define PORT
#define EXT_CLK_PIN     3       // Define pin number on PORT


/* Simulate low frequency oscillator. Pulses must be done by software */
#define EXT_LF_RES_PORT PORTA   // Define PORT
#define EXT_LF_RES_PIN  2       // Define pin number on PORT




/* Now follows definition of pins for high-voltage programming */
// Command pins
#define BS2_PORT        PORTB   // Define PORT
#define BS2_PIN         7       // Define pin number on PORT


#define PAGEL_PORT      PORTB   // Define PORT
#define PAGEL_PIN       6       // Define pin number on PORT


#define XA1_PORT        PORTB   // Define PORT
#define XA1_PIN         5       // Define pin number on PORT


#define XA0_PORT        PORTB   // Define PORT
#define XA0_PIN         4       // Define pin number on PORT


#define BS1_PORT        PORTB   // Define PORT
#define BS1_PIN         3       // Define pin number on PORT


#define WR_PORT         PORTB   // Define PORT
#define WR_PIN          2       // Define pin number on PORT


#define OE_PORT         PORTB   // Define PORT
#define OE_PIN          1       // Define pin number on PORT


#define RDY_PORT        PORTB   // Define PORT
#define RDY_PIN         0       // Define pin number on PORT


// Data pins
#define D7_PORT         PORTC   // Define PORT
#define D7_PIN          7       // Define pin number on PORT


#define D6_PORT         PORTC   // Define PORT
#define D6_PIN          6       // Define pin number on PORT


#define D5_PORT         PORTC   // Define PORT
#define D5_PIN          5       // Define pin number on PORT


#define D4_PORT         PORTC   // Define PORT
#define D4_PIN          4       // Define pin number on PORT


#define D3_PORT         PORTC   // Define PORT
#define D3_PIN          3       // Define pin number on PORT


#define D2_PORT         PORTC   // Define PORT
#define D2_PIN          2       // Define pin number on PORT


#define D1_PORT         PORTC   // Define PORT
#define D1_PIN          1       // Define pin number on PORT


#define D0_PORT         PORTC   // Define PORT
#define D0_PIN          0       // Define pin number on PORT


/* PINS FOR LCD AND MORE LCD SETTINGS ARE DEFINED AT FILE LCD_1602.h */

/* PINS FOR DC/DC CONVERTER AND MORE SETTINGS ARE DEFINED AT FILE dc_dc.h */

/*-----------------------------------------*/

/* Following lines should not be changed until You know what are You doing! */

/* Version */
#define debug   1
#define release 0

/* Error codes */
#define E_OK                    0
#define E_no_response           1
#define E_incorrect_response    2
#define E_BSY_time_out          3
#define E_unknown               0xFF

/* Oscillator types */
#define INT_OSC         1
#define EXT_XTAL_OSC    2
#define EXT_RC_OSC      3
#define EXT_CLK         4
#define EXT_LF_OSC      5
#define EXT_CLK_1_pulse 6


#if version == debug
#define F_CPU 2000000UL        /* 2MHz for debug, else should be 1MHz
                                 * Lower frequency -> lower power consumption
                                 */
#else
#define F_CPU 1000000UL        /* 2MHz for debug, else should be 1MHz
                                 * Lower frequency -> lower power consumption
                                 */
#endif

// Set RST_TARGET to logical 0
#define RST_TARGET_0    RST_TARGET_PORT = RST_TARGET_PORT & ~(1<<RST_TARGET_PIN)
// Set RST_TARGET to logical 1
#define RST_TARGET_1    RST_TARGET_PORT = RST_TARGET_PORT | (1<<RST_TARGET_PIN)


// Set KEYB_E to logical 0
#define KEYB_E_0        KEYB_E_PORT = KEYB_E_PORT & ~(1<<KEYB_E_PIN)
//Set KEYB_E to logical 1
#define KEYB_E_1        KEYB_E_PORT = KEYB_E_PORT | (1<<KEYB_E_PIN)


// Set EXT_XTAL to logical 0
#define EXT_XTAL_0      EXT_XTAL_PORT = EXT_XTAL_PORT & ~(1<<EXT_XTAL_PIN)
//Set EXT_XTAL to logical 1
#define EXT_XTAL_1      EXT_XTAL_PORT = EXT_XTAL_PORT | (1<<EXT_XTAL_PIN)


// Set EXT_RC to logical 0
#define EXT_RC_0        EXT_RC_PORT = EXT_RC_PORT & ~(1<<EXT_RC_PIN)
//Set EXT_RC to logical 1
#define EXT_RC_1        EXT_RC_PORT = EXT_RC_PORT | (1<<EXT_RC_PIN)


// Set EXT_CLK to logical 0
#define EXT_CLK_0       EXT_CLK_PORT = EXT_CLK_PORT & ~(1<<EXT_CLK_PIN)
//Set EXT_CLK to logical 1
#define EXT_CLK_1       EXT_CLK_PORT = EXT_CLK_PORT | (1<<EXT_CLK_PIN)


// Set EXT_LF_RES to logical 0
#define EXT_LF_RES_0    EXT_LF_RES_PORT = EXT_LF_RES_PORT & ~(1<<EXT_LF_RES_PIN)
//Set EXT_LF_RES to logical 1
#define EXT_LF_RES_1    EXT_LF_RES_PORT = EXT_LF_RES_PORT | (1<<EXT_LF_RES_PIN)


// Set BS2 to logical 0
#define BS2_0   BS2_PORT = BS2_PORT & ~(1<<BS2_PIN)
//Set BS2 to logical 1
#define BS2_1   BS2_PORT = BS2_PORT | (1<<BS2_PIN)


// Set PAGEL to logical 0
#define PAGEL_0 PAGEL_PORT = PAGEL_PORT & ~(1<<PAGEL_PIN)
//Set PAGEL to logical 1
#define PAGEL_1 PAGEL_PORT = PAGEL_PORT | (1<<PAGEL_PIN)


// Set XA1 to logical 0
#define XA1_0   XA1_PORT = XA1_PORT & ~(1<<XA1_PIN)
//Set XA1 to logical 1
#define XA1_1   XA1_PORT = XA1_PORT | (1<<XA1_PIN)


// Set XA0 to logical 0
#define XA0_0   XA0_PORT = XA0_PORT & ~(1<<XA0_PIN)
//Set XA0 to logical 1
#define XA0_1   XA0_PORT = XA0_PORT | (1<<XA0_PIN)


// Set BS1 to logical 0
#define BS1_0   BS1_PORT = BS1_PORT & ~(1<<BS1_PIN)
//Set BS1 to logical 1
#define BS1_1   BS1_PORT = BS1_PORT | (1<<BS1_PIN)


// Set WR to logical 0
#define WR_0    WR_PORT = WR_PORT & ~(1<<WR_PIN)
//Set WR to logical 1
#define WR_1    WR_PORT = WR_PORT | (1<<WR_PIN)


// Set OE to logical 0
#define OE_0    OE_PORT = OE_PORT & ~(1<<OE_PIN)
//Set OE to logical 1
#define OE_1    OE_PORT = OE_PORT | (1<<OE_PIN)


// Set RDY to logical 0
#define RDY_0   RDY_PORT = RDY_PORT & ~(1<<RDY_PIN)
//Set RDY to logical 1
#define RDY_1   RDY_PORT = RDY_PORT | (1<<RDY_PIN)


// Set D7 to logical 0
#define D7_0    D7_PORT = D7_PORT & ~(1<<D7_PIN)
//Set D7 to logical 1
#define D7_1    D7_PORT = D7_PORT | (1<<D7_PIN)


// Set D6 to logical 0
#define D6_0    D6_PORT = D6_PORT & ~(1<<D6_PIN)
//Set D6 to logical 1
#define D6_1    D6_PORT = D6_PORT | (1<<D6_PIN)


// Set D5 to logical 0
#define D5_0    D5_PORT = D5_PORT & ~(1<<D5_PIN)
//Set D5 to logical 1
#define D5_1    D5_PORT = D5_PORT | (1<<D5_PIN)


// Set D4 to logical 0
#define D4_0    D4_PORT = D4_PORT & ~(1<<D4_PIN)
//Set D4 to logical 1
#define D4_1    D4_PORT = D4_PORT | (1<<D4_PIN)


// Set D3 to logical 0
#define D3_0    D3_PORT = D3_PORT & ~(1<<D3_PIN)
//Set D3 to logical 1
#define D3_1    D3_PORT = D3_PORT | (1<<D3_PIN)


// Set D2 to logical 0
#define D2_0    D2_PORT = D2_PORT & ~(1<<D2_PIN)
//Set D2 to logical 1
#define D2_1    D2_PORT = D2_PORT | (1<<D2_PIN)


// Set D1 to logical 0
#define D1_0    D1_PORT = D1_PORT & ~(1<<D1_PIN)
//Set D1 to logical 1
#define D1_1    D1_PORT = D1_PORT | (1<<D1_PIN)


// Set D0 to logical 0
#define D0_0    D0_PORT = D0_PORT & ~(1<<D0_PIN)
//Set D0 to logical 1
#define D0_1    D0_PORT = D0_PORT | (1<<D0_PIN)



#endif
