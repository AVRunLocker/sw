/**
 * \file
 *
 * \brief This software should be able unblock/unlock most of AVR's so they
 * can be reprogrammed again.
 *
 * It was written for ATmega32, but should be compatible with ATmega16.
 *
 * Created:  27.09.2012\n
 * Modified: 19.06.2014
 *
 * \author Martin Stejskal - martin.stej@gmail.com
 */



#include "AVR_unLocker.h"


/*-----------------------------------------*/
/**
 * \brief Main function.
 *
 * @return If return then something is definitely wrong
 */
int main(void)
{
  // Remember: safety first!

  /* First is need to turn off pin connected to the transistor, witch control
   * DC/DC converter. This pin should be connected to the PD7, because there is
   * hardware PWM on witch is this software written
   */
  PORTD = (0<<PD7);         /* Set pin PD7 to 0 (shift 0 by 7 to the left)
                             * PORTx sets output value, or pull-up resistors
                             */
  DDRD = (1<< PD7);          // And set it as output (1 - out ; 0 - in)

  /* OK, then is good for safety turn-on transistor, witch control voltage on
   * RESET pin on target. When is this transistor on, then RESET pin on target
   * goes to low, so DC/DC can not damage target chip due to overvoltage
   * problem.
   */
  set_pin( &RST_TARGET_PORT, RST_TARGET_PIN, dir_out);  // Set pin as OUTput
  RST_TARGET_1;         // And set pin to logic 1 -> transistor on -> RST = 0V

  // Critical parts should be OK.

  // Now initialize PWM for DC/DC and ADC for DC/DC feedback
  init_DCDC_PWM_and_ADC();

  // Now initialize LCD for debug use
  LCD1602_init();               // Initialize LCD and clear LCD

  sei();                // Enable global interrupt

  // If release version -> show info
#if version == release
  LCD1602_write_text_from_flash_slow( MSG_startup_info, startup_info_delay );
  _delay_ms(user_read_quick_info_delay);       // Take user some time to read
#endif

  // And wait until DC/DC is ready
  while( DCDC_mode() == DCDC_init);



  // Check DC/DC +Vcc
  LCD1602_Clear_display();
#if version == debug
  LCD1602_write_text_from_flash( MSG_CHECKING_DCDC_VCC );
  DCDC_set_Vcc();                                // Set on DC/DC Vcc
  LCD1602_Clear_display();
  LCD1602_write_text_from_flash( MSG_CHECKING_DCDC_12V );  // If ok, show this
  DCDC_set_12V();                               // Set on DC/DC +12 V

  // Show: W8 LV (wait for low voltage on output)
  LCD1602_write_text_from_flash( MSG_W8_LV );
  DCDC_wait_for_safe_voltage();                 // Wait for low voltage on DC/DC
  LCD1602_write_text_from_flash_slow( MSG_OK, text_delay );// If ok, show this
#else
  LCD1602_write_text_from_flash_slow( MSG_CHECKING_DCDC_VCC, text_delay );
  DCDC_set_Vcc();                                // Set on DC/DC Vcc
  LCD1602_write_text_from_flash_slow( MSG_OK, text_delay );
  _delay_ms(user_read_quick_info_delay);        // Small delay
  LCD1602_Clear_display();

  // If ok, show this
  LCD1602_write_text_from_flash_slow( MSG_CHECKING_DCDC_12V, text_delay );
  DCDC_set_12V();                               // Set on DC/DC +12 V
  DCDC_wait_for_safe_voltage();                 // Wait for low voltage on DC/DC
  LCD1602_write_text_from_flash_slow( MSG_OK, text_delay );// If ok, show this
  _delay_ms(user_read_quick_info_delay);        // Small delay
#endif

  // OK, DC/DC seems to be all right, so now set I/O
  set_io_all();

  while(1)                              // Loop
    {
      /* Try find target on DIP8 thru SPI and test for OK status...
       */
      if ( Try_SPI8() == E_OK )
      {
	// If OK status, then is not needed to test other possibilities
        break;       // Maybe give while here and w8 4 keyboard
      }
      // Else is needed to test other option.
      if ( Try_SPI28() == E_OK )
      {
        // If OK status, then is not needed to test other possibilities
        break;
      }
      // Else is needed to test other option.
      if ( Try_SPI40() == E_OK )
      {
        // If OK status, then is not needed to test other possibilities
        break;
      }

      // OK, if SPI is not enough, let's try parallel high-voltage programming
      if ( Try_High_voltage_parallel() == E_OK )
      {
        break;
      }

      // Not enough? Try serial high-voltage programming
      if ( Try_High_voltage_serial() == E_OK )
      {
        break;
      }

      // Else try everything again
    }

  __asm volatile("nop\n\t"
                 "nop\n\t"
                 "nop\n\t"
                 "nop\n\t"
                 ::);

  while(1)
  {
    /* Turn on transistor witch is connected to the target reset -> RST on target
     * will get to low -> high voltage on DC/DC output should not damage target
     * chip. Just for case...
     */
    RST_TARGET_1;

#if version == debug    // Only if in debug mode ON and OFF DC/DC converter
    DCDC_set_12V();
    _delay_ms(2000);
#else
    // Turn off DC/DC
    DCDC_set_Vcc();
#endif
  }
  return(0);
}


/*-----------------------------------------*/



/* Set most inputs and outputs for proper function
 */
void set_io_all(void)
{
  set_pin( &KEYB_E_PORT,        KEYB_E_PIN,     dir_out );

  set_pin( &EXT_XTAL_PORT,      EXT_XTAL_PIN,   dir_out );
  set_pin( &EXT_RC_PORT,        EXT_RC_PIN,     dir_out );
  set_pin( &EXT_CLK_PORT,       EXT_CLK_PIN,    dir_in  );      // Default off
  set_pin( &EXT_LF_RES_PORT,    EXT_LF_RES_PIN, dir_out );

  set_pin( &BS2_PORT,           BS2_PIN,        dir_out );
  set_pin( &PAGEL_PORT,         PAGEL_PIN,      dir_out );
  set_pin( &XA1_PORT,           XA1_PIN,        dir_out );
  set_pin( &XA0_PORT,           XA0_PIN,        dir_out );
  set_pin( &BS1_PORT,           BS1_PIN,        dir_out );
  set_pin( &WR_PORT,            WR_PIN,         dir_out );
  set_pin( &OE_PORT,            OE_PIN,         dir_out );
  set_pin( &RDY_PORT,           RDY_PIN,        dir_in  );

 set_io_data_in();      /* Set data port to in for case, that target chip has
                         * set pins to output
                         */

}

/* Set data port with target (High voltage programming) to input
 */
void set_io_data_in(void)
{
  set_pin( &D7_PORT,            D7_PIN,         dir_in  );
  set_pin( &D6_PORT,            D6_PIN,         dir_in  );
  set_pin( &D5_PORT,            D5_PIN,         dir_in  );
  set_pin( &D4_PORT,            D4_PIN,         dir_in  );
  set_pin( &D3_PORT,            D3_PIN,         dir_in  );
  set_pin( &D2_PORT,            D2_PIN,         dir_in  );
  set_pin( &D1_PORT,            D1_PIN,         dir_in  );
  set_pin( &D0_PORT,            D0_PIN,         dir_in  );
}

/* Set data port with target (High voltage programming) to output
 */
void set_io_data_out(void)
{
  set_pin( &D7_PORT,            D7_PIN,         dir_out );
  set_pin( &D6_PORT,            D6_PIN,         dir_out );
  set_pin( &D5_PORT,            D5_PIN,         dir_out );
  set_pin( &D4_PORT,            D4_PIN,         dir_out );
  set_pin( &D3_PORT,            D3_PIN,         dir_out );
  set_pin( &D2_PORT,            D2_PIN,         dir_out );
  set_pin( &D1_PORT,            D1_PIN,         dir_out );
  set_pin( &D0_PORT,            D0_PIN,         dir_out );
}
