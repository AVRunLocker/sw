# target chip
device=atmega32

# optimizer settings for gcc
optimalize=Os

# fn - filename of "main" .c file
fn=AVR_unLocker

# dir_lib - user libraries folder
dir_lib=AVR_unLocker_lib

# Debug directory - for temporary compiled data.
dbg_dir=Debug

# avrdude programmer (example: usbasp, dragon_isp and so on)
programmer=dragon_isp

# avrdude command for upload program
avrdude_cmd=avrdude -c dragon_isp -U flash:w:$(fn).hex -p

# Message for "all ok"
ok_msg=echo "" ; echo "<<|| All_OK ||>>"

all: compile elf disasm hex show_ok_msg

help:
	@echo "Parameters are:"
	@echo "		all - compile all"
	@echo "		program - compile all and program target chip"
	@echo "		clean - delete all files in \"$(dbg_dir)\" directory"
	@echo "Example: make program"
	@echo " This compile all project and program target chip"

show_ok_msg:
	@$(ok_msg)
	
compile:
	@echo "Compiling files in root directiory"
	@echo ""
	@avr-gcc -Wall -$(optimalize) -std=c99 -mmcu=$(device) -c *.c \
	-I ./ -I $(dir_lib) \
	# Compile files in project root directory
	
	@echo "Compiling files in $(dir_lib)"
	@echo ""
	@avr-gcc -Wall -$(optimalize) -std=c99 -mmcu=$(device) -c \
	./$(dir_lib)/*.c \
	-I ./ -I $(dir_lib) \
	# Compile files in directory dir_lib

	@mv *.o $(dbg_dir)/ \
	# Move all *.o files to Debug directory

elf:
	@echo "Generating ELF file"
	@avr-gcc -Wall -mmcu=$(device) -o $(dbg_dir)/$(fn).elf $(dbg_dir)/*.o
	
disasm:
	@echo "Disassembling ELF"
	@avr-objdump -S $(dbg_dir)/$(fn).elf > $(dbg_dir)/$(fn).disasm

hex:
	@echo "Generating HEX file"
	@avr-objcopy -O ihex $(dbg_dir)/$(fn).elf $(dbg_dir)/$(fn).hex

	@cp $(dbg_dir)/$(fn).hex ./ \
	# And copy hex file



clean: clean_remove_files show_ok_msg

clean_remove_files:
	@echo "Deleting files in Debug folder"
	@rm -rf Debug/*


# Compile source and upload program to target chip
program: compile elf disasm hex test_4_chip show_ok_msg

test_4_chip:
# Test for ATmega32
ifeq ($(device),atmega32)
	@$(avrdude_cmd) m32
endif

ifeq ($(device),atmega16)
	@$(avrdude_cmd) m16
endif


ifneq ($(device),atmega32)
  ifneq ($(device),atmega16)
# if not found, then show this
	@echo "/----------------------------------------------------\\"
	@echo "| Target device not found in makefile database!      |"
	@echo "| Please upload program manually, or modify makefile |"
	@echo "\\----------------------------------------------------/"
	@false
  endif
endif


