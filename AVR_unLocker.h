/**
 * \file
 *
 * \brief This software should be able unblock/unlock most of AVR's so they
 * can be reprogrammed again.
 *
 * It was written for ATmega32, but should be compatible with ATmega16.
 *
 * Created:  27.09.2012\n
 * Modified: 22.06.2014
 *
 * \author Martin Stejskal - martin.stej@gmail.com
 */

#ifndef _AVR_unLocker_H_
#define _AVR_unLocker_H_

//================================| Includes |=================================
#include <avr/io.h>             // Inputs/outputs
#include <avr/pgmspace.h>       /* For work with memory. There is used for save
                                 * text to flash, because these texts are not
                                 * changing.
                                 */

#include <inttypes.h>           // Define integer types

#include "settings.h"           // Main variables for this project
#include "io_functions.h"       // Base functions with PORTs

#include <avr/interrupt.h>      // Interrupts
#include <util/delay.h>         // Delays

#include "lang.h"      // File with preset messages
#include "LCD_1602.h"  // Library for LCD 1602
#include "dc_dc.h"     // Lib. for operation with DC/DC
#include "chip_database.h"      // Simple database of supported devices

#include "clk_sources.h"        // Clock sources (crystal, ext clk and so on)
//===============================| Structures |================================
/**
 * \brief Structure for easy saving signature bytes
 *
 * It is easy to work with structure, than with many variables. When used
 * structures, code is easy to read
 */
typedef struct {
  uint8_t i_signature0;         // LSB
  uint8_t i_signature1;         // LSB+1
  uint8_t i_signature2;         // MSB
} signature_bytes_t;


//===========================| Additional includes |===========================
// SPI programming mode
#include "spi_mode.h"

// High voltage - parallel mode
#include "high_voltage_paralel.h"

// High voltage - serial mode
#include "high_voltage_serial.h"

//===========================| Function prototypes |===========================

/* Set most inputs and outputs for proper function
 */
void set_io_all(void);


/* Set data port with target (High voltage programming) to input
 */
void set_io_data_in(void);


/* Set data port with target (High voltage programming) to output
 */
void set_io_data_out(void);

#endif
